/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.FileHandling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import csci_311.Structs.Sequence;
/**
 *
 * @author Daniel
 */
public class ConfigReader{ 
    
    BufferedReader br = null;
    
    private fileManager FM = fileManager.getSingleton();
    
    public void readSequenceConfig(String location){
        try{
            String sCurrentLine;
            br = new BufferedReader(new FileReader(location + "/usecase-config.txt"));
            while ((sCurrentLine = br.readLine()) != null) {
                
                String tempClass,tempFunc;
                
                tempClass = sCurrentLine.substring(0, sCurrentLine.indexOf(';'));
                tempFunc = sCurrentLine.substring(sCurrentLine.indexOf(';')+ 1);
                Sequence tempseq = new Sequence(tempClass,tempFunc);  
                FM.addSeq(tempseq);
            }
            br.close();
        }catch(IOException e) {System.out.println(e);}
    }

}
