package csci_311;

import csci_311.Structs.*;
import csci_311.FileHandling.*;
import csci_311.UML_ClassCreator.*;
import csci_311.XMI_Builder.*;
import csci_311.GUI.*;
import java.io.IOException;
import java.io.*; 
import java.util.*;
/**
 *
 * @author Daniel
 */
public class CSCI_311 {

    /**
     * @param args the command line arguments
     */
    private static UserInterface UI = null;
    public static void main(String[] args) throws IOException {
          
        /*Model model = Model.getSingleton();
        model.setModel("modeltest");
        XMI_Manager XMLmgr = XMI_Manager.getSingleton();
        
        
        Class_XMI_Builder testclass = new Class_XMI_Builder();
        Class_XMI_Builder testclass2 = new Class_XMI_Builder();
        Class_XMI_Builder testclass3 = new Class_XMI_Builder();
        
        testclass.addClass("test");
        testclass.addAttribute("deci\'mal", "barry", "private");
        testclass.addAttribute("double", "fred", "private");
        testclass.addGeneralisation("test", "optus");
        testclass.build();
        
        testclass2.addClass("optus");
        testclass2.addAttribute("integer", "count", "private");
        testclass2.addAttribute("double", "money", "private");
        testclass2.build();
        
        String y[] = {"null", "length", "width"};
        String x[] = {"null", "dec", "byte"};
        
        testclass3.addClass("bike");
        testclass3.addAttribute("String", "alpha", "public");
        testclass3.addAttribute("double", "deci", "private");
        testclass3.addOperation("float", "foo", y, x, "public");
        testclass3.build();
        
        
        XMLmgr.addAssociation("bike", "optus", false, true);
        XMLmgr.addAssociation("test", "bike", false, false);
         
       // Output_Writer outputClassDiagram = new Output_Writer(XMLmgr.buildClassDiagram(), "test6.xmi");
        
        
        State_XMI_Builder Teststate1 =  new State_XMI_Builder("greg");
        State_XMI_Builder Teststate2 =  new State_XMI_Builder("durp");
        
        Teststate2.addEntry("entry");
        Teststate2.addExit("exit1");
        Teststate2.addExit("exit2");
        Teststate2.addState("joe");
        Teststate2.addState("hockey");
        Teststate2.addState("tennis");
        
        
        Teststate2.addLink("entry", "joe", null, "init", "start");
        Teststate2.addLink("joe", "tennis", "init", "while(stuffishappening())", "working");
        Teststate2.addLink("joe", "hockey", "run", "for(int x = 0; x < 2; x++)", "amazing");
        Teststate2.addLink("tennis", "hockey", "link", "bool = true", "sport");
        Teststate2.addLink("hockey", "exit1", "quit", null, "deconstruct");
        Teststate2.addLink("tennis", "exit2", "exit", null, "deconstruct");
        Teststate2.addLink("joe", "joe", "increment", "c < 3", "c++");
        
        Teststate1.addState("bill");
        Teststate1.addState("william");
        
        Teststate1.addLink("bill", "william", "foo", "x = true", "dostuff");
      
    
        Teststate2.build();
        Teststate1.build();
        
        Output_Writer outputDiagram = new Output_Writer(XMLmgr.buildDiagrams(), "C:\\Dell", "StateTest1.xmi");
        //System.out.println(); 
        
       /* FileFinder fFinder = new FileFinder();
        fileManager fManager = fileManager.getSingleton();
        fFinder.traverse("/home/undergrad/p/pss743/Desktop/proto/");
        ArrayList<aFile> fileList = fManager.getFileList();
       // System.out.println("ArraySize = " + fileList.size());
        for(aFile temp : fileList) {
            System.out.println("Class Name: " + temp.getClassName() + " File Path: " + temp.getFilePath());
            System.out.println("Source: \n" + temp.getContents() + "\n");
        }*/
        /*fileManager fileMan = fileManager.getSingleton();
        XML_Manager xml_manager = XML_Manager.getSingleton();
        FileFinder fileFinder = new FileFinder();
        fileFinder.traverse("C:\\test");
        classReader classreader = new classReader();
        classFinder Cfinder = new classFinder();
        ArrayList<aFile> files = fileMan.getFileList();
        for (int i = 0; i < files.size(); i++) {
            Cfinder.findClasses(files.get(i).getFilePath(),files.get(i).getContents());
        }
       /*for (aFile temp : files) {
            System.out.println(temp.getContents() + "Something Here\n\n\n");
        }*/
      /*  classreader.classreader(files);
        try {
        Output_Writer outputClassDiagram = new Output_Writer(xml_manager.buildClassDiagram(), "test6.xmi");
        }catch(IOException e) {}*/ 
       UI = new UserInterface("UML Creator");
    }
}
