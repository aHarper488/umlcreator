package csci_311.XMI_Builder;
import csci_311.Structs.*;
import java.util.Iterator;
//import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class Class_XMI_Builder extends Base_XMI_Builder {

    private Virtual_Class virt;
    
    //holds model and class name
    private String modelName;
    private String classId;
    
    //error out kill switch
    boolean killswitch = false;
    
    public Class_XMI_Builder(){
        
        super();
        
        //gets model info
        Model model = Model.getSingleton();
        modelName = model.getModel();
    }
    
    //use this on the second parse after xml manager add class has been filled
    public void addClass(String name){
        
        //purge string
        name = XMLmgr.purgeString(name);
        
        //class exist, kill it
        if(XMLmgr.checkClass(name) == true)
            killswitch =  true;
        
        XMLmgr.addClass(name);
        
        virt = new Virtual_Class(name);
        classId = name;
        
        buildString.append("  <packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_");
        buildString.append(name);
        buildString.append("\" name = \"");
        buildString.append(name);
        buildString.append("\">\n");
    }
    
    public void addAttribute(String type, String name, String scope){
    
        //error check to see if attribute is  a return value
        if(type.equals("return"))
            return;
        
        //purge strings
        type = XMLmgr.purgeString(type);
        name = XMLmgr.purgeString(name);
        
        //check if attribute exist in current virt class
        Iterator virtItr = virt.attributes.iterator();
        while(virtItr.hasNext())
            if(virtItr.next().equals(name))
                return;
               
        virt.addAttribute(name);
        XMLmgr.addDataType(type);
        
        buildString.append("    <ownedAttribute xmi:type=\"uml:Property\" xmi:id=\"Attribute_");
        buildString.append(classId);
        buildString.append(name);
        buildString.append(type);
        buildString.append("\" name = \"");
        buildString.append(name);        
        buildString.append("\" visibility = \"");       
        buildString.append(scope);
        buildString.append("\" type=\"DataType_");
        buildString.append(type); 
        buildString.append(
                        "\"/>\n");                        
    }
    
    public void addOperation(String type, String name, String[] parametersName, String[] parametersType, String scope){
           
        
        //catch an entering null
        if(name == null)
            return;
        
        //catches < and >
        type = XMLmgr.purgeString(type);
        name = XMLmgr.purgeString(name);
        String displayname = name;
        
        for(int x = 1; x < parametersName.length; x++){
            if(parametersName[x] == null)
                 break;
            
            parametersName[x] = XMLmgr.purgeString(parametersName[x]);
        }
        for(int x = 1; x < parametersType.length; x++){
            if(parametersType[x] == null)
                 break;
            
            parametersType[x] = XMLmgr.purgeString(parametersType[x]);
        }
        
        //operation exist thus is overloaded
        if(XMLmgr.checkOperation(name) == true){
            name = name + XMLmgr.getHash();
            
        }
        
        XMLmgr.addOperation(name);
        
        //add datatypes
         for(int x = 1; x < parametersType.length; x++)
            XMLmgr.addDataType(parametersType[x]);
            
         XMLmgr.addDataType(type);
         virt.addOperation(name);

         buildString.append("    <ownedOperation xmi:type=\"uml:Operation\" xmi:id=\"Operation_");
         buildString.append(classId);
         buildString.append(name);
         buildString.append("\" name = \"");
         buildString.append(displayname);
         buildString.append("\" visibility = \"");
         buildString.append(scope);
         buildString.append("\">\n"); 
         
         //check if has return type
         if(!type.equals("void")){
             
            buildString.append("      <ownedParameter xmi:type=\"uml:Parameter\" xmi:id=\"Operation_Return");
            buildString.append(classId);
            buildString.append(name);
            buildString.append(type);         
            buildString.append("\" name=\"ReturnPara\" type=\"DataType_");
            buildString.append(type);
            buildString.append("\" direction=\"return\"/>\n");
         }
         
         //add each parameter
         for(int c = 1; c < (parametersType.length); c++){
             
             String parameterID = classId + name + parametersName[c];
             
             //error checking
             if(parametersType[c] == null || parametersName[c] == null)
                 break;
            
            //operation exist thus is overloaded, hash the id
            if(XMLmgr.checkOperation(parameterID) == true){
                parameterID = parameterID + XMLmgr.getHash();
            }

            XMLmgr.addOperation(parameterID);
                 
            XMLmgr.addDataType(parametersType[c]);
            
            buildString.append("      <ownedParameter xmi:type=\"uml:Parameter\" xmi:id=\"Operation_");
            buildString.append(parameterID);
            buildString.append("\" name=\"");
            buildString.append(parametersName[c]);
            buildString.append("\" type=\"DataType_");
            buildString.append(parametersType[c]);
            buildString.append("\"/>\n");
         }
         
         buildString.append("    </ownedOperation>\n");  
  
    }
    
    //function must be called from parent class
    public void addGeneralisation(String parent, String child){
        
        //catches < and >
        parent = XMLmgr.purgeString(parent);
        child = XMLmgr.purgeString(child);
        
        StringBuilder generalisationString = new StringBuilder();
        String generalisationId = "Generalization:p_" + parent + "_c_" + child;
        
        generalisationString.append("    <generalization xmi:type=\"uml:Generalization\" xmi:id=\"");
        generalisationString.append(generalisationId);
        generalisationString.append("\" general=\"Class_");
        generalisationString.append(parent);
        generalisationString.append("\"/>\n");
    }
    
    public void buildVirtualClass(){
        
        XMLmgr.virt_classes.add(virt);// builds virtual class
    }
    
    //call this once at the end when all data is done
    //this will build the diagram
    public void build(){
        
        buildString.append("  </packagedElement> \n");
        
        this.buildVirtualClass();
        output = buildString.toString();
        
 
        XMLmgr.sendClassData(this);
    }
    
    public String getId(){
        
        return classId;
    }
    
    public Class_XMI_Builder getClassData(){         
        return this;
    }
};
    
