/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.Structs;

/**
 *
 * @author Daniel
 */
public class Sequence {
    
    private String seqClass, seqFunction;
    
    public Sequence(String parClass, String parFunction){
        
        seqClass = parClass;
        seqFunction = parFunction;       
    }
    
    public String getSeqClass(){
        return seqClass;
    }
    
    public String getSeqFunction(){
        return seqFunction;
    }
}
