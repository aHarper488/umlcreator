/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_ClassCreator;
import csci_311.XMI_Builder.Class_XMI_Builder;
import csci_311.XMI_Builder.XMI_Manager;
import csci_311.Structs.*;
import java.util.*;
import java.lang.Character;

/**
 * This class parses the files looking for attributes and operations
 * @author Phillip Smith
 */
public class classReader {
        /**
         * An instance of the singleton XML_Manager
         */
	private static XMI_Manager xml_manager = XMI_Manager.getSingleton();
        /**
        * A class_XMI_Builder that will be used several times to build each of the
        * classes
        */
        private static Class_XMI_Builder class_parser = null;
        /**
        * A string to hold the class name
        */
        private static String className = null;
        /**
        * An ArrayList that holds the names of containers to check for multiplicities
        */
        private ArrayList<String> containers = new ArrayList<String>();
        /**
         * classreader parses through a class, splitting it up and then extracting
         * the attributes and operations and passing them to the class_builder
         * @param files 
         */
	public void classreader (ArrayList<aFile> files, boolean vert) {
		containers = addContainers();
                    for (aFile source : files) {
                        try{
                        class_parser = new Class_XMI_Builder();
                        className = source.getClassName();
                        String theStuff = source.getContents();
                        String ls = System.getProperty("line.separator");
                        theStuff = theStuff.replaceAll(ls, " ");
                        theStuff = theStuff.replaceAll("\\t", " ");
                        theStuff = theStuff.trim();
                        theStuff = strip(theStuff);
                        theStuff = theStuff.replaceAll("\\=\\s*\\{\\}", " ");		
                        theStuff = theStuff.replaceAll("\\s*\\{\\s*;\\s*\\}\\s*;|\\s*\\{\\s*;\\s*\\}\\s*|\\s*\\{\\s*\\}\\s*;", "{}");
                        String [] splits = theStuff.split("(?<=[;}])");
                        getParts(splits);
                        if (!vert) {
                            class_parser.buildVirtualClass();
                        } else class_parser.build();
                        } catch(Exception e) {System.out.println("Error in " + source.getClassName());}
                    }
	}

        /**
         * Strip removes the string of unwonted information such as comments, strings,
         * characters, and the method definitions
         * @param stuff
         * @return the edited string
         */
	public String strip(String stuff) {
		int NumBr = 0;
		boolean semi_col = false;			
		Stack pos = new Stack();
                
		stuff = removeTok(stuff, '"', '"');
		stuff = removeTok(stuff, '\'', '\'');
                stuff = removeTok(stuff, "/*", "*/");
                
                //This loop will remove everything between the braces of operations
		for(int i = 0; i < stuff.length(); i++) {
			if (NumBr <= 1) {
				semi_col = false;
			}
			if(stuff.charAt(i) == '{') {
				NumBr++;
				pos.push(new Integer(i));
			} else if (stuff.charAt(i) == ';') {
				semi_col = true;
			}else if (stuff.charAt(i) == '}') {
				NumBr--;
				if (NumBr > 0) {
					int start = (Integer)pos.pop();
					if (semi_col == true){
						stuff = stuff.substring(0,start+1) + ";" +stuff.substring(i, stuff.length());
					} else { 
						stuff = stuff.substring(0,start+1) + stuff.substring(i, stuff.length());; 
					}
					if (semi_col == false) {
						i = start+1;
					} else { 
						i = start+2; 
					}
				}
			}
		}
                String parent = null;
                String FindParent = null;
                //This checks if the class inherits from another 
                FindParent = stuff.substring(stuff.indexOf(0, '{')+1);
                String s[] = FindParent.split(" ");
                for(int i= 0; i < s.length; i++) {
                    if (s[i].equals("class")) {
                        FindParent = s[i+1];
                    }
                    if(s[i].equals("extends")){
                        parent = s[i+1];
                        if(parent.lastIndexOf('{') != -1) {
                            parent = parent.substring(0, parent.lastIndexOf('{'));
                        }
                    }
                }
                
                class_parser.addClass(className);
                if (xml_manager.checkClass(parent)) {
                   
                    class_parser.addGeneralisation(parent, className);
                }
		if (stuff.charAt(stuff.length()-1) == ';') {
			stuff = stuff.substring(stuff.indexOf('{', 0)+1, stuff.length()-2);
		} else {
			stuff = stuff.substring(stuff.indexOf('{', 0)+1, stuff.length()-1);
		}
                 
                stuff = stuff.trim();
               
		return stuff;
	}
        /**
         * this method checks through each string, and passes them to either getAttribute
         * or getOperation appropriately 
         * @param strings 
         */
	public void getParts(String [] strings) {
		for (String curr : strings) {
                   try{
                       //if the string ends with a ; its an attribute
			if (curr.charAt(curr.length()-1) == ';') {
				getAttribute(curr);
                        //if it ends with a } its an operation
			} else if (curr.charAt(curr.length()-1) == '}') {
				getOperation(curr);
			}
                        //otherwise its an error
                   } catch(Exception e) {System.out.println(("Error at" + curr + " message: " + e.toString()));}
		}
	}
        
        /**
         * this function passes through a string and finds the attributes name, 
         * type, visibility and multiplicity
         * @param curr 
         */
	public void getAttribute(String curr) {
                //this loop puts spaces between all of the interesting parts of the string
		for(int i = 0; i < curr.length(); i++) {
			if((curr.charAt(i) == '=') || (curr.charAt(i) == ',') || (curr.charAt(i) == ';')) 		{
				curr = curr.substring(0, i) + " " + curr.substring(i, i+1) + " " + curr.substring(i+1, curr.length());	
					i++;
			}else if(curr.charAt(i) == '<') {
                        while (curr.charAt(i) != '>') {
                            if (curr.charAt(i) == ' ') {
                                curr = curr.substring(0, i) + curr.substring(i+1, curr.length());
                                i--;
                            } else i++;
                        }
                    }
		}
                boolean many = false;
                if(curr.indexOf('[') != -1) {
                   many = true;
                }
		curr = curr.trim();	
		curr = removeTok(curr, '(', ')');
                curr = removeTok(curr, '[', ']');
		String splits[] = curr.split("\\s+");
		
                if (splits.length < 2)
                    return;
                
		String visibility = null, type = null;
		String name[] = new String[splits.length];
		boolean found = false;
		int attrNum = 0;
		if (splits[0].equals("public")) {
			visibility = "public";
                }
                    else if(splits[0].equals("protected")){
			visibility = "protected";
                    }
				else { visibility = "private"; }
                //this for loop checks if the attribute has been initialised 
                //and reacts accordingly
		for (int i = 0; i < splits.length; i++) {
			if (splits[i].equals("=")) {
				if (type == null) 
					type = splits[i-2];
				name[attrNum] = splits[i-1];
				found = true;
			} //if it finds a , there is more than one attribute 
                        if (splits[i].equals(",")) {
				if (found == false) {
					if (type == null) {
                                            type = splits[i-2];
                                        }
					name[attrNum] =splits[i-1];
				}
				attrNum++;
				found = false;
			} //if it finds a semicolon it ends 
                        if (splits[i].equals(";")) {
				if (found == false) {
					if (type == null){						
						type = splits[i-2];
                                        }
					name[attrNum] = splits[i-1];
				}
			}
		}
                //if type contains a < then it may be a container, meaning multiplicity
                if (type.indexOf('<') != -1) {
                    if (many == false) {
                        many = checkContainers(type.substring(0,type.indexOf('<')), containers); 
                    }
                }
                //this loop checks for associations and adds them to the class builder
                if (Character.isLetter(type.charAt(0)) && (type.charAt(0) != '{')) {
                    for (int i = 0; i <= attrNum; i++) {
                            System.out.println("Attribute Name: " + name[i] + " Type: " + type);
                            if (xml_manager.checkClass(type) == true) {
                                if(xml_manager.CheckAssociation(className, type) == false) {
                                    xml_manager.addAssociation(className, type,false,many);
                                }
                            }
                                class_parser.addAttribute(type, name[i], visibility);
                    }
                }
                
	}
        
        /**
         * this function passes through a string and finds the operations name, 
         * return type, visibility and parameters
         * @param curr 
         */
	public void getOperation(String curr) {
            //this loop puts spaces between all of the interesting parts of the string
		for(int i = 0; i < curr.length(); i++) {
                    if((curr.charAt(i) == '(') || (curr.charAt(i) == ',') || (curr.charAt(i) == ';') || (curr.charAt(i) == ')')) 		{
                            curr = curr.substring(0, i) + " " + curr.substring(i, i+1) + " " + curr.substring(i+1, curr.length());	
                                    i++;
                    }else if(curr.charAt(i) == '<') {
                        while (curr.charAt(i) != '>') {
                            if (curr.charAt(i) == ' ') {
                                curr = curr.substring(0, i) + curr.substring(i+1, curr.length());
                                i--;
                            } else i++;
                        }
                    }
                }
		curr = curr.trim();
		String splits[] = curr.split("\\s+");
		
                if (splits.length <= 3) 
                    return; 
                
		String visibility = null;
		String name[] = new String[splits.length], type[] = new String[splits.length];
		boolean found = false;
		int attrNum = 0;		
	
		if (splits[0].equals("public")){ 
			visibility = "public";
                }
			else if(splits[0].equals("protected")){
				visibility = "protected";
                        }
				else {visibility = "private";}
                //this for loop searches for ( and reacts from there
		for (int i = 0; i < splits.length; i++) {
			if (splits[i].equals("(")) {
                            if (i == 1) {
                                type[attrNum] = "void";
                            } else if((!splits[i-2].equals(visibility)) && (!splits[i-2].equals("static")) 
                                    && (!splits[i-1].equals("final")))
                                    type[attrNum] = splits[i-2];
                            else type[attrNum] = "void";
                                if (!Character.isLetter(type[attrNum].charAt(0)))
                                    type[attrNum] = "void";
				name[attrNum] = splits[i-1];
				attrNum++;
			} //looks for multiple parameters 
                        if (splits[i].equals(",")) {
				type[attrNum] = splits[i-2];
                                if (!Character.isLetter(type[attrNum].charAt(0)))
                                    type[attrNum] = "void";
				name[attrNum] = splits[i-1];
				attrNum++;
			} //looks for the end of parameter list 
                        if (splits[i].equals(")") && (!splits[i-1].equals("("))) {
				type[attrNum] = splits[i-2];
                                if (!Character.isLetter(type[attrNum].charAt(0)))
                                    type[attrNum] = "void";
				name[attrNum] = splits[i-1];
			}
		}
                boolean many = false;
		System.out.println("Operation Name: " + name[0] + " Type: " + type[0] + " Visibility " + visibility);
                class_parser.addOperation(type[0], name[0], name, type,visibility);
		for (int i = 1; i <= attrNum; i++) {
			System.out.println("	Attribute Name: " + name[i] + " Type: " + type[i]);
                        if (type[i] != null) {
                            if(type[i].indexOf('[') != -1) {
                                many = true;
                            }
                        //checks for multiplicity
                        if (type[i].indexOf('<') != -1) {
                            if (many == false) {
                              many = checkContainers(type[i].substring(0,type[i].indexOf('<')), containers); 
                            }
                            type[i] = type[i].substring(type[i].indexOf('<')+1, type[i].indexOf('>'));
                        } 
                      }
                        //checks for association
                         if (xml_manager.checkClass(type[i])) {
                            if(xml_manager.CheckAssociation(className, type[i]) == false) {
                                xml_manager.addAssociation(className, type[i], false, many);
                            }
                        }
		}
	}
   
    /**
     * This function removes everything between two character delimiters and the
     * delimiters themselves
     * @param str
     * @param del
     * @param del2
     * @return the edited string
     */
        public String removeTok(String str, char del, char del2) {
		int delNo = 0;
		int start = 0;
		for (int i = 0; i < str.length() -1; i++) {
                    if ((str.charAt(i) == '/') && (str.charAt(i+1) == '*') && (delNo == 0)) {
                        while((str.charAt(i) != '*') || (str.charAt(i+1) != '/')) {
                            i++;
                        }
                    }
			if((str.charAt(i) == del) && ((str.charAt(i-1) != '\\') || ((str.charAt(i-2) == '\\') && (str.charAt(i-1) == '\\')))) {
				if((delNo == 0) || (del2 != del)){
					delNo++;
					start = i;
				} else {
					delNo--;
					str = str.substring(0, start) +  str.substring(i+1, str.length());
					i = start; 
				}				
			} else if((str.charAt(i) == del2) && (del2 != del)) {
				delNo--;
				str = str.substring(0, start) +  str.substring(i+1, str.length());
				i = start;
			}
		}
		return str;
	}
        
    /**
     * This function removes everything between two String delimiters and the
     * delimiters themselves
     * @param str
     * @param del
     * @param del2
     * @return the edited String
     */
        public String removeTok(String str, String del, String del2) {
		while((str.indexOf(del) != -1) && (str.indexOf(del2) != -1)) {
                    if ((str.indexOf(del) != 0) && (str.indexOf(del2)+2 != str.length())) {
                        str = str.substring(0, str.indexOf(del)) + str.substring(str.indexOf(del2)+2, str.length());
                    }
                    else if ((str.indexOf(del) != 0) && (str.indexOf(del2)+2 == str.length())) 
                        str = str.substring(0, str.indexOf(del));
                    else if ((str.indexOf(del) == 0) && (str.indexOf(del2)+2 != str.length())){
                        str = str.substring(str.indexOf(del2)+2, str.length());
                    }
                    else 
                        str = "";
                }
		return str;
	}
        
        /**
         * adds a list of container types to a list for checking for multiplicities
         * @return the list of container types
         */
        private ArrayList<String> addContainers() {
            ArrayList<String> con = new ArrayList<String>();
            con.add("ArrayList");
            con.add("Vector");
            con.add("Map");
            con.add("Set");
            con.add("HashMap");
            con.add("HashSet");
            con.add("Queue");
            con.add("Deque");
            con.add("List");
            return con;
        }
        
        /**
         * This method checks if the string is a container type
         * @param str
         * @param con
         * @return a boolean whether the string is a container or not
         */
        private boolean checkContainers(String str, ArrayList<String> con) {
            for(String temp : con) {
                if (temp.equals(str)) {
                    return true;
                }
            }
            return false;
        }
}

