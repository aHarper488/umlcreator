/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.FileHandling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Daniel
 */
public class FileSourceReader {
    
    private String contents;
    
    public FileSourceReader(String Path) throws IOException{
        
        contents = readFile(Path);
    }
    
    private String readFile(String file) throws IOException {
        
        BufferedReader reader = new BufferedReader( new FileReader (file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while((line = reader.readLine()) != null ){
            
            line = line.replaceAll("//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "$1 ");
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        return stringBuilder.toString();
    }
    
    public String getFile(){
        
        return contents;
    }   
}

