/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import csci_311.*;
import java.io.File;

/**
 * This class creates the user interface
 * @author Phillip Smith
 */
public class UserInterface extends JFrame{
	
	private theMenu menu = new theMenu();
	private static final UserInterface UI = new UserInterface();
        private Runner runner = new Runner();
        
	private String location;
	private String destination;
	private String output = "default.xmi";

	private JCheckBox checkCl = new JCheckBox();
	private JCheckBox checkSe = new JCheckBox();
	private JCheckBox checkSt = new JCheckBox();
	
        private JPanel panel1 = new JPanel();
        private JPanel panel2 = new JPanel();
        private JPanel panel3 = new JPanel();
        private JPanel panel4 = new JPanel();
        private JPanel panel5 = new JPanel();
        private JPanel panel6 = new JPanel();
	private JLabel labelCl = new JLabel("Create Class Diagram");
	private JLabel labelSe = new JLabel("Create Sequence Diagram");
	private JLabel labelSt = new JLabel("Create State Diagram");
	private JTextField getName = new JTextField("");
	private JButton getNameBut = new JButton("Set Output Filename");
	private JButton build = new JButton("Build");
        private JButton setLoc = new JButton("Set Source Location");
        private JButton setDes = new JButton("Set Output Destination");
        
	public UserInterface() {
	}
        /**
         * This method calls createUserInterface the user interface
         * @param message 
         */
	public UserInterface(String message) {
		UI.createUserInterface(message);
	}
        
        /**
         * this method builds the user interface and responds to selections
         * @param message 
         */
	private void createUserInterface(String message) {
		setDefaultLookAndFeelDecorated(true);
		setTitle(message);
		setLocationRelativeTo(null);
		setLayout(new GridLayout(6,0));
		setPreferredSize(new Dimension(500,400));
		setJMenuBar(menu.createMenu());
                panel1.setLayout(new GridLayout(1,2));
                panel2.setLayout(new GridLayout(1,2));
                panel3.setLayout(new GridLayout(1,2));
                panel4.setLayout(new GridLayout(1,2));
                panel5.setLayout(new GridLayout(1,2));
                panel6.setLayout(new GridLayout(1,1));
		panel1.add(labelCl);
		panel1.add(checkCl);
		panel2.add(labelSe);
		panel2.add(checkSe);
		panel3.add(labelSt);
		panel3.add(checkSt);
                panel4.add(setLoc);
                panel4.add(setDes);
		panel5.add(getName);
		panel5.add(getNameBut);
		build.setEnabled(false);
		panel6.add(build);
                
                add(panel1);
                add(panel2);
                add(panel3);
                add(panel4);
                add(panel5);
                add(panel6);
		getNameBut.addActionListener( new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					output = getName.getText();
					getName.setText("");
					if (output.length() == 0) {
						output = "default.xmi";
					}
					if (!output.endsWith(".xmi")){
						output += ".xmi";
					}
				}
			});

		checkCl.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
                                    if(location != null && destination != null) {
					if(checkCl.isSelected()) {
						build.setEnabled(true);
					}else if (!checkSe.isSelected() && !checkSt.isSelected()) {
						build.setEnabled(false);
					}
                                    }
				}
			});
			
		checkSe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
                                    if(location != null && destination != null) {
					if(checkSe.isSelected()) {
						build.setEnabled(true);
					}else if (!checkCl.isSelected() && !checkSt.isSelected()) {
						build.setEnabled(false);
					}
                                    }
				}
			});

		checkSt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
                                    if(location != null && destination != null) {
					if(checkSt.isSelected()) {
						build.setEnabled(true);
					}else if (!checkSe.isSelected() && !checkCl.isSelected()) {
						build.setEnabled(false);
					}
                                    }
				}
			});
                build.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JFrame frame = new JFrame();
                        runner.createUML(location, destination, output, checkCl.isSelected(), checkSe.isSelected(), checkSt.isSelected());
                        JOptionPane.showMessageDialog(frame, "Done!");
                    }
                });
                
                setLoc.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser chooser = new JFileChooser();
                        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                        JPanel panel = new JPanel();
			int returnVal = chooser.showOpenDialog(panel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				setLocation(file.toString());
				System.out.println(file.toString());
			} else {
				System.out.println("Cancelled");
			}
                    }
                });
                setDes.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser chooser = new JFileChooser();
                        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                        JPanel panel = new JPanel();
			int returnVal = chooser.showOpenDialog(panel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				setDestination(file.toString());
				System.out.println(file.toString());
			} else {
				System.out.println("Cancelled");
			}
                    }
                });
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}
        
        /**
         * This returns the UserInterface singleton
         * @return the UserInterface
         */
	public static UserInterface getUI() {
		return UI;
	}
	
        /**
         * This method sets the location of the source 
         * @param loc 
         */
	public void setLocation(String loc) {
		location = loc;
                if ((destination != null) && (checkCl.isSelected() || checkSe.isSelected() || checkSt.isSelected())) {
                    build.setEnabled(true);
                }
	}
        /**
         * This method sets the destination of the output file
         * @param loc 
         */
	public void setDestination(String des) {
		destination = des;
                if ((location != null) && (checkCl.isSelected() || checkSe.isSelected() || checkSt.isSelected())) {
                    build.setEnabled(true);
                }
	}
}


