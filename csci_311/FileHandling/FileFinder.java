/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.FileHandling;
import csci_311.Structs.aFile;
import java.io.*;
/**
 * Source: http://www.heimetli.ch/directory/RecursiveTraversal.html
 * this class recurses through the file system looking for java files
 * @author Phillip Smith
 */
public class FileFinder {
   /**
    * a FileSourceReader input the contents of the files into the file objects
    */
   private FileSourceReader fsr = null;
   
   /**
    * a fileManager to store the files
    */
   private static fileManager FM;
   
   /** 
    * this method calls the recursiveTravel Function
    * @param path 
    */
    public void traverse(String path) {
        File dir = new File(path);
        recursiveTraversal(dir);
    }
    /** 
    * this method calls the recursiveTravel Function
    * @param path 
    */
    public void traverse(File path) {
        recursiveTraversal(path);
    }
    
    /**
     * this function recurses through the file system from the given starting
     * point looking for java files 
     * @param dir 
     */
    private void recursiveTraversal(File dir) {
        FM = fileManager.getSingleton();
      if( dir.isDirectory() )
      {
         String entries[] = dir.list() ;

         if( entries != null )
         {
            for( String entry : entries )
            {
               traverse( new File(dir,entry) ) ;
            }
         }
      } else {
            String Dir = dir.toString();
            String Cname = dir.getName(); 
            if (Cname.length() > 5)
              Cname = Cname.substring(0, Cname.length()-5);
            if (Dir.endsWith(".java")) {
                try {
                    aFile file = new aFile();  
                    file.setFilePath(Dir);
                    file.setClassName(Cname);
                    fsr = new FileSourceReader(Dir);
                    file.setContents(fsr.getFile());
                    FM.addFile(file);
                }catch(IOException e) { System.out.println(e); }
            }
      }
    }
}

