/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311;

import csci_311.XMI_Builder.XMI_Manager;
import csci_311.Structs.*;
import csci_311.FileHandling.*;
import csci_311.UML_ClassCreator.*;
import csci_311.UML_SequenceCreator.*;
import csci_311.UML_StateCreator.*;
import japa.parser.ParseException;
import java.io.IOException;
import java.io.*; 
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author cs321lm2a
 */
public class Runner {
    private XMI_Manager xml_manager;
    private Model MetaModel;
    private FileFinder fFinder = new FileFinder();
    private ConfigReader configReader = new ConfigReader();
    private fileManager fManager;
    private classFinder Cfinder = new classFinder();
    private classReader CReader = new classReader();
    private stateReader SReader = new stateReader();
    SequenceRunner sequenceRunner = new SequenceRunner();
    public void createUML(String location, String destination, String outputFN, boolean buildCl, boolean buildSe, boolean buildSt) {
        //load singletons
        xml_manager = XMI_Manager.getSingleton();
        fManager = fileManager.getSingleton();
        MetaModel = Model.getSingleton();
        //set mode
        MetaModel.setMode(buildCl, buildSe, buildSt);
        //find files
        fFinder.traverse(location);
        configReader.readSequenceConfig(location);
        ArrayList<aFile> files = fManager.getFileList();
        for (int i = 0; i < files.size(); i++) {
            Cfinder.findClasses(files.get(i));
        }
        if(buildCl) {
            CReader.classreader(files, buildCl);
        }
            
        if (buildSe){          
            sequenceRunner.findSequence();          
        }
        
        if (buildSt) {
            if (buildCl == false){
                CReader.classreader(files, buildCl);
            }
            SReader.stateReader(files, xml_manager.virt_classes);
        }
        try {
        Output_Writer outputClassDiagram = new Output_Writer(xml_manager.buildDiagrams(), destination, outputFN);
        xml_manager.clearSingleton();
        fManager.clearSingleton();
        }catch(IOException e) {}
    }
}
