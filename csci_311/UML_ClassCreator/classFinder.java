/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_ClassCreator;
import csci_311.XMI_Builder.XMI_Manager;
import csci_311.FileHandling.*;
import csci_311.Structs.aFile;
/**
 * This class looks through the files and splits them into separate classes
 * @author Phillip Smith
 * 
 */
public class classFinder {
    /**
     * An instance of the singleton XML_Manager
     */
    XMI_Manager xml_manager = XMI_Manager.getSingleton();
    /**
     * An instance of the singleton fileManager
     */
    fileManager file_man = fileManager.getSingleton();
    /**
     * A string to hold the class name specified in the file name
     */
    String fileClassName = null;
    /**
     * A string to hold the file path of the file
     */
    String filePath = null;
    
    /**
     * This class calls the classFinder
     * @param file 
     */
    public void findClasses(aFile file) {
        classFinder(file);
    }
    /**
     * classFinder searches for multiple classes in a single file
     * and splits them into separate files by passing them to the fileManager
     * @param file 
     */
    private void classFinder(aFile file) {
        int currLen = 0;
        String Path = file.getFilePath();
        String source = file.getContents();
        String name = file.getClassName();
        try{
        //System.err.println(Path);
        source = removeTok(source, '"', '"');       //removeTok will remove the tokens and everything inbetween
        source = removeTok(source, "/*", "*/");
        String LS = System.getProperty("line.separator");
        String pattern = "(?=\\sclass\\s|" + LS + "class\\s|;class\\s|\\}class\\s)"; //this line splits the classes up
        String words[] = source.split(pattern);
        if (words.length == 1) {    //if the source is not split, check if it is a class, otherwise remove it
            aFile temp;
            if (!words[0].startsWith("class ")) {
                temp = new aFile(name, Path, "");
                file_man.addFile(temp);
            }
            return;
        }
        words[1] = removeEnd(words[1]);
        words[1] = words[1].trim();
        String mainSplits [] = words[1].split(" ");
        fileClassName = mainSplits[1];
        filePath = Path;
        //this loop cuts any inner classes out of the file and puts
        //them in their own file
        for (int i = 2; i < words.length; i++) {
            words[i] = words[i].trim();
            String splits[] = words[i].split(" ");
            if(splits.length > 1) {
                String className = fileClassName + ":" + splits[1];
                if (className.charAt(className.length()-1) == '{') {
                    className = className.substring(0, className.length()-1);
                }
                String classPath = filePath + ":" + splits[1];
                String classSource = words[i].substring(0, findEnd(words[i]));
                words[1] += words[i].substring(findEnd(words[i]), words[i].length());
                aFile temp = new aFile(className, classPath, classSource);
                file_man.addFile(temp);
                xml_manager.addClass(splits[1]);  
            }
        }
        aFile temp = new aFile(fileClassName, filePath, words[1]);
        file_man.addFile(temp);
        xml_manager.addClass(fileClassName);
        }catch(Exception e) {System.out.println("error in" + Path); //if there is an error, remove the file
             aFile temp = new aFile(name, Path, "");
           file_man.addFile(temp); 
        }
    }
    /**
     * This function returns the index of the end of the inner class 
     * @param str
     * @return the length of the string
     */
    public int findEnd(String str) {
        int NumBr = 0;
        for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == '{') {
                        NumBr++;
                } else if (str.charAt(i) == '}') {
                                NumBr--;
                                if (NumBr == 0) {
                                        return i+1;
                                }
                }
        }
        return str.length();
    }
    /**
     * This function removes anything after the last ; or } depending on which 
     * is found last
     * @param str
     * @return The shortened string
     */
    public String removeEnd(String str) {
        if(str.lastIndexOf(';') > str.lastIndexOf('}')) {
                return str.substring(0, str.lastIndexOf(';')+1);
        } else return str.substring(0, str.lastIndexOf('}')+1);
    }
    
    /**
     * This function removes everything between two character delimiters and the
     * delimiters themselves
     * @param str
     * @param del
     * @param del2
     * @return the edited string
     */
    public String removeTok(String str, char del, char del2) {
		int delNo = 0;
		int start = 0;
                //This loop makes sure the delimiters are not in a commented section
		for (int i = 0; i < str.length() -1; i++) {
                    if ((str.charAt(i) == '/') && (str.charAt(i+1) == '*') && (delNo == 0)) {
                        while((str.charAt(i) != '*') || (str.charAt(i+1) != '/')) {
                            i++;
                        }
                    }
                        //this if statement makes sure that the delimiter has not been escaped
                        //and that the escape character has not been escaped
			if((str.charAt(i) == del) && ((str.charAt(i-1) != '\\') || ((str.charAt(i-2) == '\\') && (str.charAt(i-1) == '\\')))) {
				if((delNo == 0) || (del2 != del)){
					delNo++;
					start = i;
				} else {
					delNo--;
					str = str.substring(0, start) +  str.substring(i+1, str.length());
					i = start; 
				}				
			} else if((str.charAt(i) == del2) && (del2 != del)) {
				delNo--;
				str = str.substring(0, start) +  str.substring(i+1, str.length());
				i = start;
			}
		}
		return str;
	}
    
    /**
     * This function removes everything between two String delimiters and the
     * delimiters themselves
     * @param str
     * @param del
     * @param del2
     * @return the edited String
     */
    public String removeTok(String str, String del, String del2) {
		while((str.indexOf(del) != -1) && (str.indexOf(del2) != -1)) {
                    if ((str.indexOf(del) != 0) && (str.indexOf(del2)+2 != str.length())) {
                        str = str.substring(0, str.indexOf(del)) + str.substring(str.indexOf(del2)+2, str.length());
                    }
                    else if ((str.indexOf(del) != 0) && (str.indexOf(del2)+2 == str.length())) 
                        str = str.substring(0, str.indexOf(del));
                    else if ((str.indexOf(del) == 0) && (str.indexOf(del2)+2 != str.length())){
                        str = str.substring(str.indexOf(del2)+2, str.length());
                    }
                    else 
                        str = "";
                }
		return str;
	}
}

