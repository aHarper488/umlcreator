/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_StateCreator;
import csci_311.FileHandling.*;
import csci_311.XMI_Builder.*;
import csci_311.Structs.*;
import java.io.*;
import java.util.*;
import java.lang.Character;

/**
 *
 * @author gavintreseder
 */


   
public class stateFinder {
    fileManager file_mgr = fileManager.getSingleton();
    XMI_Manager xmi_mgr = XMI_Manager.getSingleton();
    
    String className;
    String[] classCode;
    String[] attributeList;
    String[] operationList;
    public static final String[] PRIMITIVE_DATA_TYPES = {"byte", "short", "int", "long", "float", "double", "boolean", "char", "void", "enum", "String"};
    
    public stateFinder(){
        this.className = "";
    }

    public stateFinder(String cName, String[] niceCode, String[] aList, String[] oList){
        this.className = cName;
        this.classCode = niceCode;
        this.attributeList = aList;
        this.operationList = oList;
    }
    
        public stateFinder(String cName, String[] niceCode, ArrayList<String> aList, ArrayList<String> oList){
        this.className = cName;
        this.classCode = niceCode;
        this.attributeList = aList.toArray(new String[aList.size()]);
        this.operationList = oList.toArray(new String[oList.size()]);
    }

    public void getClassState(){
        State_XMI_Builder state_parser = new State_XMI_Builder(className);
        System.out.println(className);
        state_parser.addEntry("entry");
        state_parser.addState("default");
        state_parser.addLink("entry", "default", "", "", "");
        for (int a = 0; a < attributeList.length; a++){
            for (int i = 0; i < classCode.length; i++){
                if (hasAttribute(classCode[i],attributeList[a])){
                    if (isAttributeChanged(classCode[i], attributeList[a])){
                        if(isClassAttribute(i, attributeList[a])){
                            String trigger = getFunctionName(findPreviousFunctionDeclaration(i));
                            String guard = getNearestCondition(i);
                            String effect = getAttributeChange(classCode[i], attributeList[a]);     
                            if (trigger.equals(className) || trigger.equals("SystemFunction")){
                                //String oState = "default";
                                //state_parser.addState(oState);
                            }else{
                                String oState = "default";
                                String dState = attributeList[a] + effect;
                                state_parser.addState(dState);
                                state_parser.addLink(oState, dState, trigger, guard, effect);                               
                            }
                        }
                    }
                }
            }
        }
        state_parser.build();
    }

    public boolean isClassAttribute(int attLineNum, String attribute){
        int attIdx = getAttributeIndex(classCode[attLineNum], attribute);
        String front = classCode[attLineNum].substring(0, attIdx);
        
        if (front.endsWith("this.")){
            return true;
        }
        
        int lineNum = attLineNum;
        
        do{
            if (isVariableDeclared(classCode[lineNum],attribute)){
                    return false;
            }
            lineNum--;
        }while (lineNum >= 0 && isFunctionDeclaration(lineNum) == false);
        
        return true;
    }
    
    public boolean isVariableDeclared (String str, String var){
        int varIdx = getAttributeIndex (str, var);
        
        while (varIdx > 0){
            String front = str.substring(0, varIdx);
            front = front.trim();
            
            for (int i = 0; i < PRIMITIVE_DATA_TYPES.length; i++){
                if (front.endsWith(PRIMITIVE_DATA_TYPES[i])){
                    return true;
                }
            }
            varIdx = getAttributeIndex(str, var, varIdx + 1);
        }
        return false;
    }  
    
    public boolean hasAttribute (String line, String attribute){
        int attIdx = getAttributeIndex(line, attribute);
        
        if (attIdx == -1){
            return false;
        }
        return true;
    }
    
    public int getAttributeIndex(String line, String attribute){      
        return getAttributeIndex(line, attribute, 0);
    }
    
    public int getAttributeIndex(String line, String attribute, int startIdx){
        if(startIdx >= line.length()){
            return -1;
        }
        
        int attIdx = line.indexOf(attribute,startIdx);
        
        if (attIdx == 0){
            if(attIdx + attribute.length() == line.length()){
                return attIdx;
            }else if (Character.isJavaIdentifierPart(line.charAt(attIdx + attribute.length())) == false){
                return attIdx;
            }
        }
        
        if (startIdx + attribute.length() + 1 < line.length()){
            attIdx = line.indexOf(attribute, startIdx + 1);
        }
        
        while (attIdx > 0){
            if (Character.isJavaIdentifierStart(line.charAt(attIdx-1)) == false){
                if(attIdx + attribute.length() == line.length()){
                    return attIdx;
                }else if (Character.isJavaIdentifierPart(line.charAt(attIdx + attribute.length())) == false){
                    return attIdx;
                }
            }
            attIdx = line.indexOf(attribute, attIdx + 1);
        }
        
        return attIdx;
    }
    
    public boolean isAttributeChanged (String line, String attribute){
        if (getAttributeChange(line, attribute).equals("-1")){
            return false;
        }
        return true;
    }
    
    public String getAttributeChange (String line, String attribute){
        int attributeIndex = getAttributeIndex(line, attribute);
        
        if (attributeIndex >= 2){
            String prefix = line.substring(attributeIndex - 2, attributeIndex);
            if (prefix.equals("++") || prefix.equals("--")){
                return prefix;
            }
        }
        
        String suffix = line.substring(attributeIndex + attribute.length());
        
        if (suffix.length() > 0){
            if (suffix.trim().startsWith("=") == true && suffix.trim().startsWith("==") == false){
                return suffix.replaceAll(";", "").trim();
            }
        }
        
        if (suffix.length() > 1){
            if (suffix.substring(0,2).equals("++") || suffix.substring(0,2).equals("--")){
                return suffix.substring(0,2);
            }
            
            suffix = suffix.trim();
            String[] validOperators = {"+=", "-=", "*=", "/=", "%="};
            
            if (suffix.length() >=2){
                for (int i = 0; i < validOperators.length; i++){
                    if (suffix.startsWith(validOperators[i])){
                        return suffix.replaceAll(";", "").trim();
                    }
                }
            }
        }

        return "-1";
    }


    public String findPreviousFunctionDeclaration(int endLine){
        while (endLine >= 0){
            if (isFunctionDeclaration(endLine)){
                    return classCode[endLine];
            }
            endLine--;
        }
        return "SystemFunction()";
    }
    
    //needs work after parsing
    public boolean isFunctionDeclaration(int lineNum){
        if (lineNum < classCode.length - 1){
            if (classCode[lineNum + 1].equals("{")){
                if (classCode[lineNum].contains("(")){
                    String [] words = classCode[lineNum].split(" ");

                    if (words.length > 1){
                        if (isModifier(words[0]) && words[1].trim().startsWith(className)){
                            return true;
                        }
                    }

                    int i = 0;

                    while (i < words.length){
                        if (isModifier(words[i]) == false){
                            if (isDataType(words[i])){
                                return true;
                            }else {
                                return false;
                            }
                        }
                        i++;
                    }
                }
            }  
        }


        return false;
    }
    
    public boolean isDataType (String str){
        //doesn't account for arrays very well yet
        for (int i = 0; i < PRIMITIVE_DATA_TYPES.length; i++){
            if (str.equals(PRIMITIVE_DATA_TYPES[i])){
                return true;
            }else if (str.startsWith(PRIMITIVE_DATA_TYPES[i] + "[")){
                return true;
            }
        } 
        return false;
    }
    
    public boolean isModifier (String str){
        String [] modifiers = {"public", "private", "protected", "static", "final"};
        
        for (int i = 0; i < modifiers.length; i++){
            if (str.equals(modifiers[i])){
                return true;
            }
        } 
        return false;
    }
    
    public String getFunctionName (String line){
        String funcName = line.substring(0,line.indexOf("(")).trim();
        funcName = funcName.substring(funcName.lastIndexOf(" ") + 1);
        return funcName;
    }
    
    public String getNearestCondition(int attributeLineNum){
        int lineNum = attributeLineNum;
        while (isFunctionDeclaration(lineNum) == false && lineNum >= 0){
            if (classCode[lineNum].startsWith("if")|| classCode[lineNum].startsWith("else") ){
                return getIfCondition(classCode[lineNum]);
            }else if (classCode[lineNum].startsWith("switch")){
                return getSwitchCondition(lineNum, attributeLineNum);
            }else if (classCode[lineNum].equals("}")){
                lineNum = getIndexOpeningBrace(lineNum) - 1;
            }
            lineNum--;
        }
        
        return "";
    }
    
    public int getIndexOpeningBrace(int lineNum){
        int unopened = 0;
        lineNum++;
        
        do{
            lineNum--;
            if (classCode[lineNum].equals("{")){
                unopened--;
            }else if (classCode[lineNum].equals("}")){
                unopened++;
            }
        }while(lineNum > 0 && unopened > 0);
        
        return lineNum;
    }
    
    public String getIfCondition(String str){
        int start = str.indexOf("(");
        int end = str.lastIndexOf(")");
        if (start == -1 || end == -1){
            return "";
        }
        return str.substring(start,end).trim();
    }
    
    public String getSwitchCondition(int start, int end){
        int startIdx = classCode[start].indexOf("(");
        int endIdx = classCode[start].lastIndexOf(")");
        String switchCond = classCode[start].substring(startIdx + 1, endIdx);
        String returnCond = "";
        
        boolean foundCase = false;
        boolean moreCase = true;
        
        while (end > start && moreCase == true){
            String line = classCode[end];
            if (line.startsWith("case ")){
                if (foundCase){
                    returnCond = (" or ").concat(returnCond);
                }
                String cond = line.substring(4, line.indexOf(":")).trim();
                returnCond = (switchCond + " = " + cond).concat(returnCond);
                foundCase = true;
            }else if (foundCase == true){
                moreCase = false;
            }
            end--;
        }
        
        return returnCond;      
    }

}

