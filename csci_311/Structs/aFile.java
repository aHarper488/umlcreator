/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.Structs;

/**
 * This class holds the files
 * @author Phillip Smith
 */
public class aFile {
    /**
     * strings to hold the class name, file path and contents 
     */
    private String ClassName, filePath, contents;
    
    /**
     * constructor to initialize the contents of the file to null
     */
    public aFile() {
        ClassName = null;
        filePath = null;
        contents = null;
    }
     /**
      * constructor to initialize the strings
      * @param cn
      * @param fp
      * @param ct 
      */
    public aFile(String cn, String fp, String ct){
        ClassName = cn;
        filePath = fp;
        contents = ct;
    }
    /**
     * set the class name
     * @param name 
     */
    public void setClassName(String name) {
        ClassName = name;
    }
    
    /**
     * set the file path
     * @param pth 
     */
    public void setFilePath(String pth) {
        filePath = pth;
    }
    
    /**
     * set the contents
     * @param cts 
     */
    public void setContents(String cts) {
        contents = cts;
    }
    /**
     * get the class name
     * @return 
     */
    public String getClassName() {
        return ClassName;
    }
    /**
     * get the file path
     * @return 
     */
    public String getFilePath() {
        return filePath;
    }
    /**
     * get the contents
     * @return 
     */
    public String getContents() {
        return contents;
    }
    
}

