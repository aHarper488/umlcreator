/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.XMI_Builder;

import csci_311.Structs.*;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
/**
 *
 * @author Daniel 
 */
public class XMI_Manager {
    
    private static XMI_Manager xmlManager; // the static variable for the object
    
    //model data
    private Model model;
    private String title = "UOW2013Autumn_CSCI311_group_4_exporter";
    private int hashIndex = 0;
   
    //class
    //error checking list
    public ArrayList<Virtual_Class> virt_classes = new ArrayList<Virtual_Class>();
    private ArrayList<String> classes = new ArrayList<String>();
    private ArrayList<Association> associations = new ArrayList<Association>();
    private ArrayList<String> operations = new ArrayList<String>();
    private ArrayList<String> lstDatatypes = new ArrayList<String>();

    //states
    private ArrayList<String> states = new ArrayList<String>();
    private ArrayList<String> transitions = new ArrayList<String>();
    //sequence
    private ArrayList<String> swimlanes = new ArrayList<String>();
    private ArrayList<String> messages = new ArrayList<String>();
    
    //build strings
    //diagram meta data
    private StringBuffer EventString = new StringBuffer();
    private StringBuffer seqeuenceClassString = new StringBuffer();
    private StringBuffer strDataTypes = new StringBuffer();
   
    //Diagram list
    private ArrayList<String> lstClassDiagrams = new ArrayList<String>();
    private ArrayList<String> lstStateDiagrams = new ArrayList<String>();
    private ArrayList<String> lstSequenceDiagrams = new ArrayList<String>();
    
    public XMI_Manager(){
        
        model = Model.getSingleton();
    }
    
    public void clearSingleton(){
        
        xmlManager = new XMI_Manager();
    }
    
    public String getHash(){
        
        hashIndex++;
        return Integer.toString(hashIndex);
    }
    
    //purges string for parser breaking characters
    public String purgeString(String str){
        
        if(str == null)
            return null; 
        //replaces 
        str = str.replaceAll(" ", "_");
            
        //paragraph
        //str = str.replaceAll("\\r", "");
        str = str.replaceAll("\r", "");
        //str = str.replaceAll("\\n", "");
        str = str.replaceAll("\n", "");

        //catches tab
        str = str.replaceAll("\\t", "__");
        
        //catches "
        str = str.replaceAll("\"", "&apos;");
      
        //catches " "
        str = str.replaceAll(" ", "_");
        
        //catches &
        str = str.replaceAll("&", "&amp;");
   
        //catches " and '
        str = str.replaceAll("'", "&apos;");
        str = str.replaceAll("\"", "&quot;");
                
        //catches < and >
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("<", "&lt;");

        
        return str;
    }
    
   //add all classes here in first parse
    public void addClass(String name){
        
        classes.add(name);
    }
    
    //returns true if class exist
    public boolean checkClass(String name) throws ConcurrentModificationException{
        
        Iterator classesItr = classes.iterator();
        while(classesItr.hasNext())
            if(classesItr.next().equals(name))
                return true;
  
       return false;
    }
    
    public void addOperation(String name){
        operations.add(name);
    }
    
    //checks if operation exist in model
     public boolean checkOperation(String id){
        
        Iterator operationsItr = operations.iterator();
        while(operationsItr.hasNext())
            if(operationsItr.next().equals(id))
                return true;
  
        return false;
    }

    public void addAssociation(String parent, String child, boolean bParentMultiplicty, boolean bChildMultiplicty){
       
        Association addAss = new Association(parent, child, bParentMultiplicty, bChildMultiplicty, true);
        associations.add(addAss);
    }
    
     //will check parent to child and child to parent associations
    //returns true if association exist
    public boolean CheckAssociation(String parent, String child){
       
        Iterator associationsItr =  associations.iterator();
        
        while(associationsItr.hasNext()){
            
            Association checkAss = (Association)associationsItr.next();
            
            //duplicate return true
            if(checkAss.parent.equals(parent) && checkAss.child.equals(child))
                return true;
            
            //association alrady made but in reverse thus asscoation is not aggregate any more
            if(checkAss.parent.equals(child) && checkAss.child.equals(parent)){
                
                checkAss.aggregate = false;
                return true;
            }
                
        }
         
        return false;
    }
    
    public void addState(String id){
        
        states.add(id);
    }
    
    //returns true if state exist
    public boolean checkState(String id){
        
        Iterator statesItr = states.iterator();
        while(statesItr.hasNext())
            if(statesItr.next().equals(id))
                return true;
  
        return false;

    }
    
    public void addTransition(String id){
        
        transitions.add(id);
    }
    
    //returns true if transition already exist
    public boolean checkTransition(String id){
        
        Iterator transitionsItr = transitions.iterator();
        while(transitionsItr.hasNext())
            if(transitionsItr.next().equals(id))
                return true;
        
        return false;
    }
    
    public void addSwimLane(String id){
        
        swimlanes.add(id);
    }
    
    //returns true if swimlane id already exist
    public boolean checkSwimlane(String id){
        
        Iterator swimlanesItr = swimlanes.iterator();
        while(swimlanesItr.hasNext())
            if(swimlanesItr.next().equals(id))
                return true;
        
        return false;
    }
    
    public void addMessage(String id){

        messages.add(id);
    }
    
    //returns true if message id already exist
    public boolean checkMessage(String id){
        
        Iterator messagesItr = messages.iterator();
        while(messagesItr.hasNext())
            if(messagesItr.next().equals(id))
                return true;
        
        return false;
    }
    
     //builds all associations
    public String buildAssociations(StringBuilder builStr){
        
        //quick fix
        StringBuilder strFinalAssociations = new StringBuilder();
        Iterator associationsItr = associations.iterator();
        int assCount = 1;
        int roleCount = 1;
        
        
        while(associationsItr.hasNext()){        
            //quickfix 
            StringBuilder strAssociations = new StringBuilder();
            Association ass = (Association)associationsItr.next();
            
            if(!checkConsistancyquickfix(ass, builStr))
                continue;
            
            //catches < and >
            ass.parent =  purgeString(ass.parent);
            ass.child = purgeString(ass.child);

            strAssociations.append("  <packagedElement xmi:type=\"uml:Association\" xmi:id=\"Association_");
            strAssociations.append(assCount);
            strAssociations.append("\" name = \"");
            strAssociations.append(ass.child);
            strAssociations.append("_to_");
            strAssociations.append(ass.parent);
            strAssociations.append("\" memberEnd=\"Association_");
            strAssociations.append(ass.parent);
            strAssociations.append("_to_");
            strAssociations.append(ass.child);
            strAssociations.append(" Association_");
            strAssociations.append(ass.child);
            strAssociations.append("_to_");
            strAssociations.append(ass.parent);
            strAssociations.append("\"/>\n");
            
            
            //insert association to built class diagrams
             String searchStr;
             String InputString;
             int searchIndex;
             
/*for parent*/
            //string to search for, input after it
            searchStr = "<packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_"  + ass.parent + "\" name = \"" + ass.parent +"\">";
            
            //input this string
            InputString = 
                    "    <ownedAttribute xmi:type=\"uml:Property\" xmi:id=\"Association_" + ass.parent + "_to_" + ass.child +  "\" name=\"" + ass.parent + "_to_" +
                    ass.child + "\" visibility=\"private\" type=\"Class_" + ass.child + "\" ";
            
            //aggregatetion
            if(ass.aggregate == true)
                InputString = InputString + "aggregation=\"composite\" " ; 
            
            //finish input string
            InputString = InputString + "association=\"Association_" + assCount + "\">\n" + "    </ownedAttribute>\n";
            
            //find the offset
            searchIndex = builStr.indexOf(searchStr);
            searchIndex = builStr.indexOf(">", searchIndex) + 2;
            builStr.insert(searchIndex, InputString);
            
/*for child*/
            //string to search for, input after it
            searchStr = "<packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_"  + ass.child  + "\" name = \"" + ass.child +"\">";
            //input this string
            InputString = 
                    "    <ownedAttribute xmi:type=\"uml:Property\" xmi:id=\"Association_" + ass.child + "_to_" + ass.parent + "\" name=\"" + ass.child + "_to_" +
                    ass.parent + "\" visibility=\"private\" type=\"Class_" + ass.parent + "\" association=\"Association_" + assCount + "\">\n" +
                    "    </ownedAttribute>\n";           
            
            //find the offset
            searchIndex = builStr.indexOf(searchStr);
            searchIndex = builStr.indexOf(">", searchIndex) + 2;                    
            builStr.insert(searchIndex, InputString);
            
           strFinalAssociations.append(strAssociations.toString());
            
            assCount++;
        }
        
        return strFinalAssociations.toString();
    }
    
    public boolean checkConsistancyquickfix(Association ass1, StringBuilder builStr){
             String searchStr;
             String InputString;
             int searchIndex;
             
        searchStr = "<packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_"  + ass1.child  + "\" name = \"" + ass1.child +"\">";
        searchIndex = builStr.indexOf(searchStr);
        if(searchIndex < 50)
                return false;
        
        searchStr = "<packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_"  + ass1.parent + "\" name = \"" + ass1.parent +"\">";
        searchIndex = builStr.indexOf(searchStr);
        if(searchIndex < 50)
                return false;
        
        return true;
    }
    
    public void addDataType(String datatype){
        
        //error check
        if(datatype == null)
            return;
        
        //checks if datatype already exist
        Iterator lstDatatypesItr = lstDatatypes.iterator();
        while(lstDatatypesItr.hasNext())
            if(lstDatatypesItr.next().equals(datatype))
                return; //found match
        
        //carry on if not
        lstDatatypes.add(datatype);
        
        strDataTypes.append("    <packagedElement xmi:type=\"uml:PrimitiveType\" xmi:id=\"DataType_");
        strDataTypes.append(datatype);  
        strDataTypes.append("\" name=\"");
        strDataTypes.append(datatype);
        strDataTypes.append("\"/>\n");
                     
    }
    
    public void addEvent(String name, String id){
        
        EventString.append("  <packagedElement xmi:type=\"uml:CallEvent\" xmi:id=\"Event_");
        EventString.append(id);        
        EventString.append("\" name=\"");        
        EventString.append(name);        
        EventString.append("\" operation=\"StateOperation_");   
        EventString.append(id); 
        EventString.append("\"/>\n"); 
    }
    
    //add class for sequence diagrams that do not exist in the class diagram
    public void addSequenceClass(String className){

        seqeuenceClassString.append("  <packagedElement xmi:type=\"uml:Class\" xmi:id=\"Class_");
        seqeuenceClassString.append(className);
        seqeuenceClassString.append("\" name = \"");
        seqeuenceClassString.append(className);
        seqeuenceClassString.append("\"/>\n");

    }
   
    public void sendClassData(Class_XMI_Builder xmlClass){
       
        lstClassDiagrams.add(xmlClass.getOutput());
    }
   
    //adds sequence data to list of sequence data
    public void sendSequenceData(Sequence_XMI_Builder xmlSequence){
       
        lstSequenceDiagrams.add(xmlSequence.getOutput());
    }
   
    //add state data to list of state data
    public void sendStateData(State_XMI_Builder xmlState){
       
        lstStateDiagrams.add(xmlState.getOutput());
    }
    
    //builds all diagrams
    public String buildDiagrams(){
    
        StringBuilder outputBuilder = new StringBuilder();
        
        outputBuilder.append(
                        "<?xml version = '1.0' encoding = 'UTF-8' ?>\n" +
                        "<uml:Model xmi:version=\"2.1\"  xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\" xmlns:uml=\"http://schema.omg.org/spec/UML/2.2\" xsi:schemaLocation=\"http://schema.omg.org/spec/UML/2.2 http://www.eclipse.org/uml2/3.0.0/UML\" xmi:id=\"");
        outputBuilder.append(model.getModel());
        outputBuilder.append("\" name=\"");
        outputBuilder.append(model.getModel());
        outputBuilder.append("\">>\n");
        outputBuilder.append(
                        "  <packageImport xmi:type=\"uml:PackageImport\" xmi:id=\"_N_fRgPkaEeKtRdSedaVXyA\">\n" +
                        "    <importedPackage xmi:type=\"uml:Model\" href=\"http://schema.omg.org/spec/UML/2.2/uml.xml#_0\"/>\n" +
                        "  </packageImport>\n");
        
        //add class diagrams
        Iterator lsClassDiagramsItr = lstClassDiagrams.iterator();
        while(lsClassDiagramsItr.hasNext())
            outputBuilder.append((String)lsClassDiagramsItr.next());
        
        //add sequence diagrams
        Iterator lstSequenceDiagramsItr = lstSequenceDiagrams.iterator();
        while(lstSequenceDiagramsItr.hasNext())
            outputBuilder.append((String)lstSequenceDiagramsItr.next());
        
        //add state diagrams
        Iterator lstStateDiagramsItr = lstStateDiagrams.iterator();
        while(lstStateDiagramsItr.hasNext())
            outputBuilder.append((String)lstStateDiagramsItr.next());
        
        //add associations
        if(model.checkClassBuild())
            outputBuilder.append(buildAssociations(outputBuilder));
        
        //add datatypes
        if(model.checkClassBuild()){
            outputBuilder.append("  <packagedElement xmi:type=\"uml:Package\" xmi:id=\"PrimitiveDataTypes\" name=\"PrimitiveDataTypes\">\n");
            outputBuilder.append(strDataTypes);
            outputBuilder.append("  </packagedElement>\n");
        }
        
        //add events
        if(model.checkStateBuild())
            outputBuilder.append(EventString);
        
        //add sequence specific class (for robustness)
        outputBuilder.append(seqeuenceClassString);
        
        outputBuilder.append(
                        "  <profileApplication xmi:type=\"uml:ProfileApplication\" xmi:id=\"_ofZ-oPnhEeKtRdSedaVXyA\">\n" +
                        "    <xmi:Extension extender=\"http://www.eclipse.org/emf/2002/Ecore\">\n" +
                        "      <eAnnotations xmi:type=\"ecore:EAnnotation\" xmi:id=\"_ofZ-ofnhEeKtRdSedaVXyA\" source=\"http://www.eclipse.org/uml2/2.0.0/UML\">\n" +
                        "        <references xmi:type=\"ecore:EPackage\" href=\"http://schema.omg.org/spec/UML/2.2/StandardProfileL2.xmi#_yzU58YinEdqtvbnfB2L_5w\"/>\n" +
                        "      </eAnnotations>\n" +
                        "    </xmi:Extension>\n" +
                        "    <appliedProfile xmi:type=\"uml:Profile\" href=\"http://schema.omg.org/spec/UML/2.2/StandardProfileL2.xmi#_0\"/>\n" +
                        "  </profileApplication>\n" +
                        "</uml:Model>");
        
        return outputBuilder.toString(); 
    };

    // the singleton function which allows you to create the object when it is initially called, otherwise it returns the reference to the object.
    public static XMI_Manager getSingleton()
    {
            if(xmlManager == null)
            {
                    xmlManager = new XMI_Manager();
            }

            return xmlManager;
    }
}
