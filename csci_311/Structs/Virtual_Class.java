
package csci_311.Structs;

import java.util.ArrayList;

/**
 *
 * @author da488
 */
public class Virtual_Class {
    
    public String className;
    
    public ArrayList<String> attributes = new ArrayList();  
    public ArrayList<String> functions = new ArrayList();
    
   public Virtual_Class (String name){
       
       className = name;
   }
   
   public void addAttribute(String name){
       attributes.add(name);
   }
   
   public void addOperation(String name){
       functions.add(name);
   }
    
}
