/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.FileHandling;

import csci_311.Structs.*;
import java.util.*;
/**
 * This class manages the list of files
 * @author Phillip Smith
 */
public class fileManager {
    /**
     * The static instance of fileManager
     */
    private static fileManager fManager;
    
    /**
     * an ArrayList to hold the files
     */
    private ArrayList<aFile> fileList = new ArrayList<aFile>();
    
    /**
     * an array list of all sequence diagrams read by usecase-config.txt
     */
    private ArrayList<Sequence> seqList = new ArrayList<Sequence>();
    
    /**
     * a method to get the singleton fileManager
     * @return the instance of fileManager
     */
    public static fileManager getSingleton() {
        if (fManager == null) {
            fManager = new fileManager();
        }
        return fManager;
    }
    
    /**
     * This method clears the old singleton
     */
    public void clearSingleton(){
        fManager = new fileManager();
    }
    
    /**
     * This method adds files to the fileList. If the file is already in the
     * file list, it is overridden
     * @param file 
     */
    public void addFile(aFile file) {
        if (fileList.isEmpty()) {
            fileList.add(file);
            return;
        } else {
            for(int i = 0; i < fileList.size(); i++) {
                if ((fileList.get(i)).getClassName().equals(file.getClassName())) {
                     fileList.set(i, file);
                     return;
                }
                else if((fileList.get(i)).getFilePath().equals(file.getFilePath())) {
                     fileList.set(i,file);
                     return;
                }
            }
        }
        fileList.add(file);
    }
    
    /**
     * Returns the file list
     * @return the file list
     */
    public ArrayList<aFile> getFileList() {
        return fileList;
    }
    
    
    /*
     * adds new seq onto the list
     */
    public void addSeq(Sequence seq){
        seqList.add(seq);
    }
    
    /*
     * gets entire list
     */
    public ArrayList<Sequence> getSeqList(){
        return seqList;
    }
}

