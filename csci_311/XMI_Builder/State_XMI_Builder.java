/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.XMI_Builder;

/**
 *
 * @author Daniel
 * 
 * Class usage:
 * 
 * pubic class example{
 * 
 *  boolean x;
 *  boolean y;
 *  boolean z;
 * };
 * 
 * Becomes:
 * 
 * addState("x");
 * addState("y");
 * addState("z");
 * addState("yz");
 * addState("xz");
 * addState("yx");
 * addState("xyz");
 * addState("NULL");
 * 
 * 
 *  addLink("x", "yx", "y", "True");
 *  addLink("yx", "xyz", "z", "True");
 *  addLink("xyz","xz, "y", "False");
 * 
 * ect for all possible combinations IE bool cubed (3 x 3 x 3);
 */

public class State_XMI_Builder extends Base_XMI_Builder{
    
    //holds states till build
    private StringBuffer StateString = new StringBuffer();
    //holds transitions till uild
    private StringBuffer TransitionString = new StringBuffer();
    //holds owned operations till build
    private StringBuffer OperationString = new StringBuffer();
    
    private String diagramName = null;
    private String entryName = null;
    private String exitName = null;
    
    //name must be unique
    public State_XMI_Builder(String name){
        
        super();
        diagramName = XMLmgr.purgeString(name);
              
    }
    
    
    public void addEntry(String name){
        
        name = XMLmgr.purgeString(name);
        
        //check for unique id
        if(XMLmgr.checkState("State:" + diagramName + name)){
            
            System.out.println("ERROR: Unable to create state as State:" + diagramName + name + " already exist");
            return;
        }
        
        XMLmgr.addState("State:" + diagramName + name);
        
        entryName = name;
        StateString.append("      <subvertex xmi:type=\"uml:Pseudostate\" xmi:id=\"State_");
        StateString.append(diagramName);
        StateString.append(name);
        StateString.append("\"/>\n");   
    }
    
    public void addExit(String name){
        
        name = XMLmgr.purgeString(name);
        
        //check for unique id
        if(XMLmgr.checkState("State:" + diagramName + name)){
            
            System.out.println("ERROR: Unable to create state as State:" + diagramName + name + " already exist");
            return;
        }
        
        XMLmgr.addState("State:" + diagramName + name);
        StateString.append("      <subvertex xmi:type=\"uml:FinalState\" xmi:id=\"State");
        StateString.append(diagramName);
        StateString.append(name);
        StateString.append("\"/>\n");
    }
    
    public void addState(String name){
      
        
        name = XMLmgr.purgeString(name);
        
        //check for unique id
        if(XMLmgr.checkState("State:" + diagramName + name)){
            
            System.out.println("ERROR: Unable to create state as State:" + diagramName + name + " already exist");
            return;
        }
        
        XMLmgr.addState("State:" + diagramName + name);
        
        StateString.append("      <subvertex xmi:type=\"uml:State\" xmi:id=\"State_");
        StateString.append(diagramName);
        StateString.append(name);
        StateString.append("\" name=\"");
        StateString.append(name);
        StateString.append("\"/>\n");
   
    }
    
    public void addLink(String originState, String destState, String trigger, String guard, String effect){
       
        //does link have an effect?
        if(effect.equals(""))
            return;
        
        //paragraph
        originState = originState.replaceAll("\n", "");
        destState = destState.replaceAll("\n", "");
        trigger = trigger.replaceAll("\n", "");
        
        //catches tab
        originState = originState.replaceAll("\t", "__");
        destState = destState.replaceAll("\t", "__");
        trigger = trigger.replaceAll("\t", "__");
        
        //catches "
        originState = originState.replaceAll("\"", "&apos;");
        destState = destState.replaceAll("\"", "&apos;");
        trigger = trigger.replaceAll("\"", "&apos;");
        
        //catches " "
        originState = originState.replaceAll(" ", "_");
        destState = destState.replaceAll(" ", "_");
        trigger = trigger.replaceAll(" ", "_");
        guard = guard.replaceAll(" ", "_");
        effect = effect.replaceAll(" ", "_");
        
        //catches &
        originState = originState.replaceAll("&", "&amp;");
        destState = destState.replaceAll("&", "&amp;");
        trigger = trigger.replaceAll("&", "&amp;");
        guard = guard.replaceAll("&", "&amp;");
        effect = effect.replaceAll("&", "&amp;");
        
        //catches " and '
        originState = originState.replaceAll("'", "&apos;");
        originState = originState.replaceAll("\"", "&quot;");
        destState = destState.replaceAll("'", "&apos;");
        destState = destState.replaceAll("\"", "&quot;");
        trigger = trigger.replaceAll("'", "&apos;");
        trigger = trigger.replaceAll("\"", "&quot;");
        guard = guard.replaceAll("'", "&apos;");
        guard = guard.replaceAll("\"", "&quot;");
        effect = effect.replaceAll("'", "&apos;");
        effect = effect.replaceAll("\"", "&quot;");
 
        //catches < and >
        originState = originState.replaceAll(">", "&gt;");
        originState = originState.replaceAll("<", "&lt;");
        destState = destState.replaceAll(">", "&gt;");
        destState = destState.replaceAll("<", "&lt;");
        trigger = trigger.replaceAll(">", "&gt;");
        trigger = trigger.replaceAll("<", "&lt;");
        effect = effect.replaceAll(">", "&gt;");
        effect = effect.replaceAll("<", "&lt;");
        
        //add local refrenceing
        originState = diagramName + originState;
        destState = diagramName + destState;
        
        String name = originState + "_to_" + destState;
        
        if(XMLmgr.checkTransition("Transition:" + name)){
            
            System.out.println("ERROR: Unable to create state link as Transition:" + name + " already exist");
            return;
        }
        
        
        XMLmgr.addTransition("Transition:" + name);
        
        //build transition
        TransitionString.append("      <transition xmi:type=\"uml:Transition\" xmi:id=\"Transition_");
        TransitionString.append(name);        
        TransitionString.append("\" name=\"");
        TransitionString.append(trigger);        
        TransitionString.append("()\" source=\"State_");
        TransitionString.append(originState); 
        TransitionString.append("\" target=\"State_"); 
        TransitionString.append(destState);
        TransitionString.append("\"/>\n");
        
        //if no other attrbiutes end the string
      /*  if(guard.equals("") && effect.equals("") && trigger.equals("")){
            TransitionString.append("\"/>\n"); 
            return;
        }
        //close current line
        else
            TransitionString.append("\">\n");
        
        //if transition has a guard
       /* if(!guard.equals("")){
           
            guard = guard.replaceAll(">", "&gt;");
            guard = guard.replaceAll("<", "&lt;"); 
            
            
            TransitionString.append("       <ownedRule xmi:type=\"uml:Constraint\" xmi:id=\"TransitionGuard_");
            TransitionString.append(name);
            TransitionString.append("\" constrainedElement=\"Transition_");   
            TransitionString.append(name);
            TransitionString.append("\">\n");
            TransitionString.append("          <specification xmi:type=\"uml:OpaqueExpression\" xmi:id=\"TransitionGuardExp_");        
            TransitionString.append(name);        
            TransitionString.append("\">\n");              
            TransitionString.append(
                                "            <language>Analysis</language>\n" +
                                "            <body>[");
            TransitionString.append(guard);
            TransitionString.append(
                                "]</body>\n" +
                                "          </specification>\n" +
                                "        </ownedRule>\n");        
        }
        
        //if the transition has an effect
        if( !effect.equals("")){
            
            TransitionString.append("        <effect xmi:type=\"uml:OpaqueBehavior\" xmi:id=\"TransitionEffect_");
            TransitionString.append(name); 
            TransitionString.append("\" name=\"");    
            TransitionString.append(effect);              
            TransitionString.append("\"/>\n");

        }
        
        //transition has a trigger
        if(!trigger.equals("")){
        
            String triggerID = trigger + XMLmgr.getHash();
            XMLmgr.addEvent(trigger, triggerID);    

            TransitionString.append("        <trigger xmi:type=\"uml:Trigger\" xmi:id=\"TransitionLocalTrigger_");
            TransitionString.append(triggerID);
            TransitionString.append("\" name=\"");  
            TransitionString.append(trigger);
            TransitionString.append("\" event=\"Event_");
            TransitionString.append(triggerID);      
            TransitionString.append("\"/>\n");
            
            //add to operation string
            OperationString.append("    <ownedOperation xmi:type=\"uml:Operation\" xmi:id=\"StateOperation_");
            OperationString.append(triggerID);
            OperationString.append("\" name=\"");
            OperationString.append(trigger);
            OperationString.append("\"/>\n");
 
        }   */       
        

       
        //TransitionString.append("      </transition>\n"); 
    }
        
        //origin
       /* addTransitionToState(originState, name, false);
        if(originState.equals(entryName)){
            
            TransitionString.append("<UML:Pseudostate xmi.idref = 'InitalState:");
            TransitionString.append(originState);      
            TransitionString.append("'/>\n");
        }
        else{
            
            TransitionString.append("<UML:SimpleState xmi.idref = 'State:");
            TransitionString.append(originState);      
            TransitionString.append("'/>\n");
        }
        
        TransitionString.append("</UML:Transition.source>\n"); 
        
        //dest
        addTransitionToState(destState, name, true);
        TransitionString.append("<UML:Transition.target>\n"); 
        if(originState.equals(exitName)){
            
            TransitionString.append("<UML:Pseudostate xmi.idref = 'InitalState:");
            TransitionString.append(destState);      
            TransitionString.append("'/>\n");
        }
        else{
            
            TransitionString.append("<UML:SimpleState xmi.idref = 'State:");
            TransitionString.append(destState);      
            TransitionString.append("'/>\n");
        }
        
        TransitionString.append("</UML:Transition.target>\n"); 

               
        TransitionString.append("</UML:Transition>\n");   */            

    //private
    //incoming =    if the transition is entring the state = true 
    //              if leaving the state false                
    private void addTransitionToState(String stateName, String id, boolean incoming){
        
        int index;
        String transition =  "<UML:Transition xmi.idref = 'Transition:" + id + "'/>\n";
        //find state heading
        index = StateString.indexOf("xmi.id = 'State:" + stateName + "'");
        
        if(incoming){

            //finds transition heading within state
            index = StateString.indexOf("<UML:StateVertex.incoming>", index);
            //26 = length of "<UML:StateVertex.outgoing>" as index of return starts of string when we need the end
            index +=  27;
            StateString.insert(index, transition);
        }
        else{
            
            //finds transition heading within state
            index = StateString.indexOf("<UML:StateVertex.outgoing>", index);
            //26 = length of "<UML:StateVertex.outgoing>" as index of return starts of string when we need the end
            index +=  27;
            StateString.insert(index, transition);
        }
    }
   
    public void build(){
        
        StringBuilder outputBuilder = new StringBuilder();

        //header
        outputBuilder.append("  <packagedElement xmi:type=\"uml:StateMachine\" xmi:id=\"State_");
        outputBuilder.append(diagramName);
        outputBuilder.append("\" name=\"");
        outputBuilder.append(diagramName);
        outputBuilder.append("\">\n"); 
        
        //add owned operations
        outputBuilder.append(OperationString.toString());
        
        //add region
        outputBuilder.append("    <region xmi:type=\"uml:Region\" xmi:id=\"StateRegion_");
        outputBuilder.append(diagramName);
        outputBuilder.append("\" name=\"Region:");
        outputBuilder.append(diagramName);
        outputBuilder.append("\">\n");
        
        //add states
        outputBuilder.append(StateString.toString());
        
        //add transitions
        outputBuilder.append(TransitionString.toString());
        
        outputBuilder.append(
                        "    </region>\n" +
                        "  </packagedElement>\n");

        output = outputBuilder.toString();
        XMLmgr.sendStateData(this);
    }
    
    public State_XMI_Builder getStateDate(){
        return this;
    }
    
}
