/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.GUI;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * This class creates the drop down menu
 * @author Phillip Smith
 */

public class theMenu extends JPanel implements ActionListener {
	private JMenuItem setLocation = new JMenuItem("Set Source location");

	private JMenuItem setDestination = new JMenuItem("Set output destination");	

	private JMenuItem exit = new JMenuItem("Exit");

	private JFileChooser chooser = new JFileChooser();
        
        private UserInterface UI;
        
        /**
         * This method adds the values to the drop down menu
         * @return the menu
         */
	public JMenuBar createMenu() {
                UI = UserInterface.getUI();
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		setLocation.addActionListener(this);
		setDestination.addActionListener(this);
		exit.addActionListener(this);
		menu.add(setLocation);
		menu.add(setDestination);
		menu.add(exit);
		menuBar.add(menu);
		return menuBar;
	}
        
        /**
         * this method checks which option was selected and reacts accordingly
         * @param event 
         */
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == setLocation) {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = chooser.showOpenDialog(theMenu.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				UI.setLocation(file.toString());
				System.out.println(file.toString());
			} else {
				System.out.println("Cancelled");
			}
		}else if(event.getSource() == setDestination) {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = chooser.showOpenDialog(theMenu.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				UI.setDestination(file.toString());
				System.out.println(file.toString());
			} else {
				System.out.println("Cancelled");
			}
		} else if(event.getSource() == exit) {
			System.exit(0);
		}
	}

	protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = theMenu.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
