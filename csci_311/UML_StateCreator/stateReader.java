/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_StateCreator;
import csci_311.FileHandling.*;
import csci_311.XMI_Builder.*;
import csci_311.Structs.*;
import java.io.*;
import java.util.*;
/**
 *
 * @author gavintreseder
 */

public class stateReader {
    String inputCode;
    String classCode[];
    
    public void stateReader(ArrayList<aFile> files, ArrayList<Virtual_Class> classList){
        
        for (aFile source : files) {
            String fileClassName = source.getClassName();
            if (fileClassName.equals("ASTParser")){
                
            }
            inputCode = source.getContents();
            classCode = treatCode(inputCode);
            System.out.println(fileClassName);
            for (Virtual_Class vClass : classList){
                if (vClass.className.equals(fileClassName)){
                    stateFinder sFinder = new stateFinder(fileClassName, classCode, vClass.attributes, vClass.functions);
                    sFinder.getClassState();
                }
            }
        }
        System.out.println("done reading");
    }
    
    public String[] treatCode(String rawCode){
        ArrayList<String> progressCode = new ArrayList<String>();
        rawCode = removeTok(rawCode, '\"', '\"', "...");
        rawCode = removeTok(rawCode, '\"', '\"', "...");
        rawCode = removeTok(rawCode, "/*", "*/", "...");
        
        String ls = System.getProperty("line.separator");
        rawCode = rawCode.replaceAll(ls, " ");

        int pos = 0;

        while (pos < rawCode.length()){
            char ch = rawCode.charAt(pos);
            switch (ch){
                case ';':
                    progressCode.add(rawCode.substring(0, pos + 1).trim());
                    rawCode = rawCode.substring(pos + 1);
                    pos = 0;
                    break;
                case '(':
                    int end = getIndexClosingBracket(rawCode, pos);
                    if (end > 0){
                        
                    }
                    progressCode.add(rawCode.substring(0, end + 1).trim());
                    rawCode = rawCode.substring(end + 1);
                    pos = 0;
                    break;
                case '{':
                case '}':
                    progressCode.add(rawCode.substring(0, pos).trim());
                    progressCode.add(rawCode.substring(pos, pos + 1).trim());
                    rawCode = rawCode.substring(pos + 1);
                    pos = 0;
                    break;
                default:
                    pos++;
             }
        }
        
        if (rawCode.length() > 0){
            progressCode.add(rawCode);
        }
        
        progressCode.removeAll(Arrays.asList("", null));
        
        String[] formattedCode = new String[progressCode.size()];
        formattedCode = progressCode.toArray(formattedCode);
        
        return formattedCode;
    }

    public int getIndexClosingBracket(String code, int pos){
        return getIndexClosingToken(code, pos, '(', ')');
    }
    
    public int getIndexClosingToken(String code, int pos, char open, char close){
        int unclosed = 0;
        pos--;
        
        do{
            pos++;
            if (code.charAt(pos) == open){
                unclosed++;
            }else if (code.charAt(pos) == close){
                unclosed--;
            }
        }while(pos < code.length() - 1 && unclosed > 0);
        
        return pos;
    }
    
            public String removeTok(String str, char del, char del2, String replacement) {
		int delNo = 0;
		int start = 0;
		for (int i = 0; i < str.length() -1; i++) {
                    if ((str.charAt(i) == '/') && (str.charAt(i+1) == '*') && (delNo == 0)) {
                        while((str.charAt(i) != '*') || (str.charAt(i+1) != '/')) {
                            i++;
                        }
                    }
			if((str.charAt(i) == del) && ((str.charAt(i-1) != '\\') || ((str.charAt(i-2) == '\\') && (str.charAt(i-1) == '\\')))) {
				if((delNo == 0) || (del2 != del)){
					delNo++;
					start = i;
				} else {
					delNo--;
					str = str.substring(0, start) +  replacement + str.substring(i+1, str.length());
					i = start; 
				}				
			} else if((str.charAt(i) == del2) && (del2 != del)) {
				delNo--;
				str = str.substring(0, start) + replacement + str.substring(i+1, str.length());
				i = start;
			}
		}
		return str;
	}
        
        public String removeTok(String str, String del, String del2, String replacement) {
		while((str.indexOf(del) != -1) && (str.indexOf(del2) != -1)) {
                    if ((str.indexOf(del) != 0) && (str.indexOf(del2) + del2.length() != str.length())) {
                        str = str.substring(0, str.indexOf(del)) + replacement + str.substring(str.indexOf(del2) + del2.length(), str.length());
                    }
                    else if ((str.indexOf(del) != 0) && (str.indexOf(del2) + del2.length() == str.length())) 
                        str = str.substring(0, str.indexOf(del)) + replacement;
                    else if ((str.indexOf(del) == 0) && (str.indexOf(del2) + del2.length() != str.length())){
                        str = replacement + str.substring(str.indexOf(del2) + del2.length(), str.length());
                    }
                    else 
                        str = "";
                }
		return str;
	}
}

