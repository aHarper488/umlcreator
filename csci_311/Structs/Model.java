/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.Structs;

/**
 *
 * @author Daniel
 * singleton for getting the model
 */
public class Model {
    
    private static Model model; // the static variable for the object
    private String strModel = "default";
    
    //holds which diagrams to make
    private boolean makeClass;
    private boolean makeSequence;
    private boolean makeState;
    
    public void setModel(String inputModel){
        
        strModel = inputModel;
    }
    public String getModel(){
        
        return strModel;
    }
    
    public void setMode(boolean classDia, boolean stateDia, boolean sequenceDia){
        
        makeClass = classDia;
        makeSequence = stateDia;
        makeState = sequenceDia;
    }
    
    public boolean checkClassBuild(){
        return makeClass;
    }
    
    public boolean checkSequenceBuild(){
        return makeSequence;
    }
    
    public boolean checkStateBuild(){
        return makeState;
    }
    
    //singlton constructor
    public static Model getSingleton(){
        
            if(model == null){
                
                    model = new Model();
            }

            return model;
    }
}
