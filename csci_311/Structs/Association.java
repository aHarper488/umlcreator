/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.Structs;

/**
 *
 * @author Daniel
 */
public class Association {
    public String parent;
    public String child;
    public boolean parentMultiplicty;
    public boolean childMultiplicty;
    public boolean aggregate;
    
    public Association(String strParent, String strChild, boolean bParentMultiplicty, boolean bChildMultiplicty, boolean bAggregate){
        
        parent = strParent;
        child = strChild;
        parentMultiplicty = bParentMultiplicty;
        childMultiplicty = bChildMultiplicty;
        aggregate = bAggregate;
    }
}
