/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.XMI_Builder;
import csci_311.Structs.*;

/**
 *
 * @author Daniel
 */
public class Sequence_XMI_Builder extends Base_XMI_Builder {
    
    private StringBuffer ClassString = new StringBuffer();
    private StringBuffer swimLaneString = new StringBuffer(); //contains swimlane info
    private StringBuffer ownedConnector = new StringBuffer(); //contains owned connectors
    private StringBuffer seqProp = new StringBuffer(); //contains owned sequence properties
    private StringBuffer messageString = new StringBuffer(); // contains message info
    private StringBuffer fragmentString = new StringBuffer(); //contains owned sequence fragements
    private StringBuffer interactionString = new StringBuffer(); //contains colaberation info
    
    int order;
    String diagramName;
    String diagramDisplayName;
    String predecessor;
    
    //holds ordering
    String previousFragment;
    String currentLifeline;
    
    Model model;
    
    //true if module is corrupt
    boolean errFlag;
    
    public Sequence_XMI_Builder(String title){
        
        super();
        
        model = Model.getSingleton();
        
        order = 0;
        errFlag = false;
        diagramDisplayName = title;
        diagramName = title + XMLmgr.getHash(); //tmp
        previousFragment = "";
        currentLifeline = "";
    }
    
    public void addSwimlane(String name, String strClass){
        
        //catches < and >
       /* strClass = strClass.replaceAll(">", "&gt;");
        strClass = strClass.replaceAll("<", "&lt;");
        
        name = name.replaceAll(">", "&gt;");
        name = name.replaceAll("<", "&lt;");*/
        
        name = XMLmgr.purgeString(name);
        strClass = XMLmgr.purgeString(strClass);
            
        if(XMLmgr.checkSwimlane("Lifeline:" + name)){
            
            //System.err.println("ERROR: Unable to create swimlane as SwimLane:" + name + " already exist");
            return;
        }
        //holds id for swimlane
        String id = strClass + "_" + name;
        
        //add
        XMLmgr.addSwimLane("Lifeline:" + name);
        
        //build swimlane main string
        swimLaneString.append("      <lifeline xmi:type=\"uml:Lifeline\" xmi:id=\"Lifeline_");
        swimLaneString.append(name);
        swimLaneString.append("\" name=\"");
        swimLaneString.append(name);
        swimLaneString.append("\" represents=\"SeqProp_");
        swimLaneString.append(id);

        swimLaneString.append("\" coveredBy=\"");
        swimLaneString.append("");
        swimLaneString.append("\"/>\n");

        //build connector string
        ownedConnector.append("        <end xmi:type=\"uml:ConnectorEnd\" xmi:id=\"ConnectorEnd_");
        ownedConnector.append(id);
        ownedConnector.append("\" role=\"SeqProp_");
        ownedConnector.append(id);
        ownedConnector.append("\"/>\n");
        
        //build sequence property
        seqProp.append("    <ownedAttribute xmi:type=\"uml:Property\" xmi:id=\"SeqProp_");
        seqProp.append(id);
        seqProp.append("\" name=\"");
        seqProp.append(name);
        seqProp.append("\" type=\"Class_");
        seqProp.append(strClass);
        seqProp.append("\"/>\n");
        
        //if not building classes
        if(model.checkClassBuild() == false){
            XMLmgr.addClass(strClass);
            XMLmgr.addSequenceClass(strClass);
        }
        // if class doesnt exist in model add it
        else if(!XMLmgr.checkClass(strClass)){        
            XMLmgr.addClass(strClass);
            XMLmgr.addSequenceClass(strClass);
        }
    }

        
        
        
        //old
/*        swimLaneString.append("<UML:ClassifierRole xmi.id = 'SwimLane:");
        swimLaneString.append(name);
        swimLaneString.append("' name = '");
        swimLaneString.append(name);
        swimLaneString.append(
                        "' isSpecification = 'false' isRoot = 'false' isLeaf = 'false' isAbstract = 'false'>\n" +
                        "<UML:ClassifierRole.base>\n");
        //add class refrence
        swimLaneString.append("<UML:Class xmi.idref = 'Class:");
        swimLaneString.append(strClass);
        swimLaneString.append(
                        "'/>\n" +
                        "</UML:ClassifierRole.base>\n");

        swimLaneString.append("</UML:ClassifierRole>\n");
        
        //check if class exist in model add to model if it doesnt exist
        if(!XMLmgr.checkClass(strClass)){
            XMLmgr.addClass(strClass); //add class
            XMLmgr.addSequenceClass(strClass); //add class to diagram
        }/*/
    
    //send message
    public void sendMessage(String functionName, String strReturn, String originSwimlane, String destinationSwimlane){

        functionName = XMLmgr.purgeString(functionName);
        originSwimlane = XMLmgr.purgeString(originSwimlane);
        destinationSwimlane = XMLmgr.purgeString(destinationSwimlane);
        strReturn = XMLmgr.purgeString(strReturn); 
        
        String id = diagramName + "_" + functionName;
        boolean isRet;
        // if no return
        if(strReturn == null)
            isRet = false;
        else
            isRet = true;
              
        if(XMLmgr.checkMessage("SendMessage:" + id)){
            
            System.out.println("ERROR: Unable to create mesaage as SendMessage:" + functionName + " already exist");
            return;
        }
        
        XMLmgr.addMessage("SendMessage:" + id);
        
        //if not first fragment
        if(!previousFragment.equals("")){
            linkFragments("sendEvent_" + id, originSwimlane);
        }
        
        messageString.append("      <message xmi:type=\"uml:Message\" xmi:id=\"SendMessage_");
        messageString.append(id);
        messageString.append("\" name=\"");
        messageString.append(functionName);
        if(!isRet)
            messageString.append("\" messageSort=\"asynchCall");
        messageString.append("\" receiveEvent=\"receiveEvent_");
        messageString.append(id);
        messageString.append("\" sendEvent=\"sendEvent_");
        messageString.append(id);
        messageString.append("\" connector=\"OwnedConnector_");
        messageString.append(diagramName);
        messageString.append("\"/>\n");
        
        //sender
        addFragemnt("sendEvent_" + id, originSwimlane, "SendMessage_" + id);
       
        //receiever
        addFragemnt("receiveEvent_" + id, destinationSwimlane, "SendMessage_" + id);
        
        //event and lifeline we finish in
        previousFragment = "receiveEvent_" + id;
        currentLifeline = destinationSwimlane;

        //if has return
        if(isRet){
            this.returnMessage(strReturn, destinationSwimlane, originSwimlane);
        }
                
    }
    
    private void linkFragments(String finish, String lifeline){
        
        String id = "link_" + previousFragment + "_" + finish;
        
        fragmentString.append("      <fragment xmi:type=\"uml:BehaviorExecutionSpecification\" xmi:id=\"");
        fragmentString.append(id);
        fragmentString.append("\" covered=\"Lifeline_");
        fragmentString.append(lifeline);
        fragmentString.append("\" start=\"");
        fragmentString.append(previousFragment);
        fragmentString.append("\" finish=\"");
        fragmentString.append(finish);
        fragmentString.append("\"/>\n");
        
         addCoveredBy(lifeline, id);
    }
    
    private void addFragemnt(String id, String covered, String message){
        
        fragmentString.append("      <fragment xmi:type=\"uml:MessageOccurrenceSpecification\" xmi:id=\"");
        fragmentString.append(id);
        fragmentString.append("\" covered=\"Lifeline_");
        fragmentString.append(covered);
        fragmentString.append("\" message=\"");
        fragmentString.append(message);
        fragmentString.append("\"/>\n");
        
        addCoveredBy(covered, id);
        
    }
    
        private void addCoveredBy(String lifeline, String fragmentId){
       
        int searchIndex = 0;

        //find lifeline
        searchIndex = swimLaneString.indexOf("xmi:id=\"Lifeline_" + lifeline);
        //fined coveredby then adds its length to insert at the end of coveredby
        searchIndex = (swimLaneString.indexOf("coveredBy=\"", searchIndex) + "coveredBy=\"".length());
        //insert
        System.out.println("inserting fragment: |" + fragmentId);
        
        try{
            swimLaneString.insert(searchIndex, fragmentId + " ");
        }
        catch(java.lang.StringIndexOutOfBoundsException ex){
            errFlag = true;
        }
        
        
        
        //here why?

    }
    
    //return message
    //copy paste of send message with slight varience
    private void returnMessage(String functionName, String originSwimlane, String destinationSwimlane){
        
        if(XMLmgr.checkMessage("ReturnMessage:" + functionName)){
            
            System.out.println("ERROR: Unable to create mesaage as ReturnMessage:" + functionName + " already exist");
            return;
        }
        
        XMLmgr.addMessage("ReturnMessage:" + functionName);
        String id = diagramName + "_" + functionName;
        
        messageString.append("      <message xmi:type=\"uml:Message\" xmi:id=\"SendMessage_");
        messageString.append(id);
        messageString.append("\" name=\"");
        messageString.append(functionName);
        messageString.append("\" messageSort=\"reply\" receiveEvent=\"receiveEvent_");
        messageString.append(id);
        messageString.append("\" sendEvent=\"sendEvent_");
        messageString.append(id);
        messageString.append("\" connector=\"OwnedConnector_");
        messageString.append(diagramName);
        messageString.append("\"/>\n");
        
        //sender
        addFragemnt("sendEvent_" + id, originSwimlane, "SendMessage_" + id);
       
        //receiever
        addFragemnt("receiveEvent_" + id, destinationSwimlane, "SendMessage_" + id);
        
        //event and lifeline we finish in
        previousFragment = "receiveEvent_" + id;
        currentLifeline = destinationSwimlane;
    }
    

    
   /* private void addInteraction(String functionName, String originSwimlane, String destinationSwimlane, boolean isreturn){
        
        //catches < and >
        functionName = functionName.replaceAll(">", "&gt;");
        functionName = functionName.replaceAll("<", "&lt;");     
        originSwimlane = originSwimlane.replaceAll(">", "&gt;");
        originSwimlane = originSwimlane.replaceAll("<", "&lt;");
        destinationSwimlane = destinationSwimlane.replaceAll(">", "&gt;");
        destinationSwimlane = destinationSwimlane.replaceAll("<", "&lt;");
        
        interactionString.append("<UML:Message xmi.id = 'Message:");
        interactionString.append(functionName);
        interactionString.append("' name = '");
        interactionString.append(functionName);
        interactionString.append(
                    "' isSpecification = 'false'>\n" +
                    "<UML:Message.sender>\n" +
                    "<UML:ClassifierRole xmi.idref = 'SwimLane:");
        interactionString.append(originSwimlane);
        interactionString.append(
                    "'/>\n" +
                    "</UML:Message.sender>\n" +
                    "<UML:Message.receiver>\n" +
                    "<UML:ClassifierRole xmi.idref = 'SwimLane:");
        interactionString.append(destinationSwimlane);
        interactionString.append(
                    "'/>\n" +
                    "</UML:Message.receiver>\n");
        
        //add predessor message (ordering)
        if(predecessor != null){
            interactionString.append(
                    "<UML:Message.predecessor>\n" + 
                    "<UML:Message xmi.idref = '");
            interactionString.append(predecessor);
            interactionString.append(
                    "'/>\n" +
                    "</UML:Message.predecessor>\n");
   
        }
        interactionString.append("<UML:Message.action>\n");
        
        //if iteraction is a return      
        if(isreturn){
            interactionString.append("<UML:ReturnAction xmi.idref = 'ReturnMessage:");
            interactionString.append(functionName);
        }  
        //is a return
        else{
            interactionString.append("<UML:SendAction xmi.idref = 'SendMessage:");
            interactionString.append(functionName);
        }
        
        interactionString.append(
                    "'/>\n" +
                    "</UML:Message.action>\n" +
                    "</UML:Message>\n");      
    }
    
    /*Function start is the function right after the loop starts
     * Function end is the last function in the loop
     * Condition is the loop rules
     * 
     * ie:
     * 
     * doStuff();
     * 
     * for(int i = 0;i < 10; i++){
     *      doStuff1();
     *      doStuff2();
     *      doStuff3();
     * };
     * 
     * doStuff4();
     * 
     * Becomes
     * 
     * addLoop("dostuff1()", "dostuff3()", "int i = 0;i < 10; i++");
     */
    public void addLoop(String functionStart, String functonEnd, String condition){
        
    }
    
    /* Function is the function that has the constraint
     * constraint is the condition IE if(x == true) becomes 'x == 
     *if(x == true)
     *  dostuff();
     * 
     * becomes
     * 
     * addConstraint("dostuff()", "x == true");
     */
    public void addConstraint(String function, String constraint){
        
    }
    
    public void build(){
        
        //if diagram has no data or is corrupt, do not build
        if(swimLaneString.length() == 0 || errFlag == true)
            return;
        
        buildString.append("  <packagedElement xmi:type=\"uml:Collaboration\" xmi:id=\"Collaboration_");
        buildString.append(diagramName);
        buildString.append("\" name=\"");
        buildString.append(diagramDisplayName);
        buildString.append(
                        "\">\n" +
                        "    <ownedBehavior xmi:type=\"uml:Interaction\" xmi:id=\"Interaction:");
        buildString.append(diagramName);
        buildString.append(
                        "\">\n" +
                        "      <ownedConnector xmi:type=\"uml:Connector\" xmi:id=\"OwnedConnector_");
        buildString.append(diagramName);
        buildString.append("\">\n");                   
        buildString.append(ownedConnector);
        buildString.append("      </ownedConnector>\n");
        buildString.append(swimLaneString);
        buildString.append(fragmentString);
        buildString.append(messageString);
        buildString.append("    </ownedBehavior>\n");
        buildString.append(seqProp);
        buildString.append("  </packagedElement>\n");
  
        output = buildString.toString();
        XMLmgr.sendSequenceData(this);
        
        
        /*buildString.append(
                    "' isSpecification = 'false' isRoot = 'false' isLeaf = 'false' isAbstract = 'false'>\n" +
                    "<UML:Namespace.ownedElement>\n");
        buildString.append(swimLaneString);
        buildString.append(messageString);
        buildString.append(
                    "</UML:Namespace.ownedElement>\n" +
                    "<UML:Collaboration.interaction>\n" +
                    "<UML:Interaction xmi.id = 'Interaction:");
        buildString.append(diagramName);
        buildString.append(
                    "' name = 'Interactions' isSpecification = 'false'>\n" +
                    "<UML:Interaction.message>\n");
        
        buildString.append(interactionString);  
        
        
        ///old
        
        buildString.append("<UML:Collaboration xmi.id = 'Sequence:");
        buildString.append(diagramName);
        buildString.append("' name = '");
        buildString.append(diagramName);
        buildString.append(
                    "' isSpecification = 'false' isRoot = 'false' isLeaf = 'false' isAbstract = 'false'>\n" +
                    "<UML:Namespace.ownedElement>\n");
        buildString.append(swimLaneString);
        buildString.append(messageString);
        buildString.append(
                    "</UML:Namespace.ownedElement>\n" +
                    "<UML:Collaboration.interaction>\n" +
                    "<UML:Interaction xmi.id = 'Interaction:");
        buildString.append(diagramName);
        buildString.append(
                    "' name = 'Interactions' isSpecification = 'false'>\n" +
                    "<UML:Interaction.message>\n");
        
        buildString.append(interactionString);
        buildString.append(
                    "</UML:Interaction.message>\n" +
                    "</UML:Interaction>\n" +
                    "</UML:Collaboration.interaction>\n" +
                    "</UML:Collaboration>\n");*/
    
    }
    
    public Sequence_XMI_Builder getSequenceData(){   
        return this;
    } 
}
