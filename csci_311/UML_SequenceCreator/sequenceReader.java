/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_SequenceCreator;

import csci_311.FileHandling.FileFinder;
import csci_311.FileHandling.fileManager;
import csci_311.Structs.*;
import csci_311.XMI_Builder.Sequence_XMI_Builder;

import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Josh
 */
public class sequenceReader {
        private Sequence_XMI_Builder parser; 
        private fileManager FM = fileManager.getSingleton();
        private ArrayList<aFile> filelist = FM.getFileList();
        private CallMethodVisitor callvisitor = new CallMethodVisitor();
        private MainCallMethodVisitor maincallvisitor = new MainCallMethodVisitor();
        private MethodDeclarationVisitor declarationvisitor = new MethodDeclarationVisitor();
        private ContentMethodVisitor contentvisitor = new ContentMethodVisitor();
        private InstanceMethodVisitor instancevisitor = new InstanceMethodVisitor(); 
        private ArrayList<String> mainfunctions = new ArrayList();
        private String MainClassName;
        private String MainClassContent;
        private String MainClassPath;
        private String MainFunc;
        private CompilationUnit cu = null;
        private CompilationUnit cu2 = null;
        private CompilationUnit cu3 = null;
        private String Function;
        private String FunctionPath;
        private String FunctionName;
        InputStream in;
        boolean check=false;
        private ArrayList<String> pattern = new ArrayList();
        private int count=0;
        public void findSequence(aFile seqFile, String StartFunc) throws FileNotFoundException, ParseException{
            
            parser = new Sequence_XMI_Builder(StartFunc);
            
            //modified
            MainClassName= seqFile.getClassName();
            MainClassContent= seqFile.getContents();
            MainClassPath= seqFile.getFilePath();
            
            try{   
                in = new FileInputStream(MainClassPath);
                cu = japa.parser.JavaParser.parse(in);
                contentvisitor.setStartFunc(StartFunc);
                contentvisitor.visit(cu, null);
                MainFunc="class "+MainClassName+" { public void func()"+contentvisitor.getMain()+"}";
            }
            catch(japa.parser.ParseException | java.lang.NullPointerException | java.io.FileNotFoundException jp){
                
            }

            
            try{
                //orginal code
                in = new ByteArrayInputStream(MainFunc.getBytes());
                cu = japa.parser.JavaParser.parse(in);
                maincallvisitor.visit(cu, null);
                mainfunctions = maincallvisitor.getFunctions();
            }
            catch(japa.parser.ParseException | java.lang.NullPointerException jp){
                
            }
            
            try{
                         
                //Pass though main functions
                in = new FileInputStream(MainClassPath);
                cu = japa.parser.JavaParser.parse(in);
                for(int i=0; i<mainfunctions.size(); i++){
                    String[] parts=mainfunctions.get(i).split("\\.");
                    if("this".equals(parts[0])){

                        this.SequenceFinder(parts[1].trim(),"main", "main", MainClassName, MainClassName);

                        }else{
                            String[] functionsplit=mainfunctions.get(i).split("\\.");
                            instancevisitor.visit(cu, parts[0]);
                            if(instancevisitor.getClassName()==null){
                            List<TypeDeclaration> content=cu.getTypes();
                            for(int m=0; m<content.size(); m++){
                                for(int d=0; d<content.get(m).getMembers().size(); d++){
                                    if(content.get(m).getMembers().get(d).toString().contains(functionsplit[0].trim()+" = new")){
                                        String[] instancesplit=content.get(m).getMembers().get(d).toString().split(" = new");
                                        instancesplit = instancesplit[1].split("\\(");
                                        this.SequenceFinder(functionsplit[1].trim(),"main" ,functionsplit[0].trim(), MainClassName, instancesplit[0].trim());
                                    }
                                }
                            }
                        }else{
                            String Classname=instancevisitor.getClassName();
                            instancevisitor.setnull();
                            this.SequenceFinder(functionsplit[1].trim(),"main" ,functionsplit[0].trim(), MainClassName, Classname.trim());    

                            }
                     }
                 }
            }
            catch(japa.parser.ParseException | java.lang.NullPointerException | java.io.FileNotFoundException jp){
                
            }
            
            parser.build();
        }
      
        //Recursive function finder
        ArrayList<ArrayList<String>> list = new ArrayList();
        public void SequenceFinder(String function, String FromObject, String ToObject, String FromClass, String ToClass) throws ParseException, FileNotFoundException{
                    
                    for(int i=0; i<pattern.size(); i++){
                        if(pattern.get(i).equals(function)){
                        count++;
                       }
                    }
                    
                    if(pattern.contains(function)){
                    }else{
                         pattern.add(function);
                   System.out.print("///////////Function: "+function+ ", From class: "+FromClass+", Going to class : "+ ToClass+'\n');
                   
                    parser.addSwimlane(ToClass, ToClass);
                    parser.addSwimlane(FromClass, FromClass);
                    parser.sendMessage(function, null, FromClass, ToClass);
                    
                    for(int i=0; i<filelist.size(); i++){
                        if(filelist.get(i).getClassName().equals(ToClass)&&!"classReader".equals(ToClass)){
                        String FunctionName=filelist.get(i).getClassName();
                        String Function=filelist.get(i).getContents();
                        String FunctionPath=filelist.get(i).getFilePath();
                        InputStream input = new FileInputStream(FunctionPath);
                        cu3 = japa.parser.JavaParser.parse(input);
                        declarationvisitor.visit(cu3, function);
                        String func = "class "+MainClassName+" { public void func()"+declarationvisitor.getFunc()+"}";
                        
                        InputStream input2 = new ByteArrayInputStream(func.getBytes());
                        cu2 = japa.parser.JavaParser.parse(input2);
                        callvisitor.visit(cu2, null);
                        ArrayList<String> temp=new ArrayList();
                        temp=callvisitor.getFunctions();
                        list.add(new ArrayList(temp));
                        
                        for(int p=0; p<list.get(list.size()-1).size(); p++){
                             String[] parts=list.get(list.size()-1).get(p).split("\\.");
                             if("this".equals(parts[0])){
                                 if(count>10){
                                    count=0;
                                 }else{
                                    this.SequenceFinder(parts[1].trim(),"main", "main", FunctionName.trim(), FunctionName.trim());
                                 }
                             }else{
                                 String[] functionsplit=list.get(list.size()-1).get(p).split("\\.");
                                 instancevisitor.visit(cu2, parts[0]);/// change to search function vs whole class
                                 if(instancevisitor.getClassName()==null){
                                 List<TypeDeclaration> content=cu3.getTypes();
                                 for(int m=0; m<content.size(); m++){
                                 for(int d=0; d<content.get(m).getMembers().size(); d++){
                                    if(content.get(m).getMembers().get(d).toString().contains(functionsplit[0].trim()+" = new")){
                                        String[] instancesplit=content.get(m).getMembers().get(d).toString().split(" = new");
                                        instancesplit = instancesplit[1].split("\\(");
                                        if(count>10){
                                           count=0;
                                        }else{
                                            this.SequenceFinder(functionsplit[1].trim(),"main" ,functionsplit[0].trim(), FunctionName.trim(), instancesplit[0].trim());
                                        } 
                                    }
                                }
                                }
                                 }else{
                                     String Classname=instancevisitor.getClassName();
                                     instancevisitor.setnull();
                                     if(count>10){
                                        count=0;
                                     }else{
                                        
                                        this.SequenceFinder(functionsplit[1].trim(),"main" ,functionsplit[0].trim(), FunctionName.trim(), Classname.trim());    
                                     }
                                 }          
                             }
                            
                          }
                        
                      }
                   }   
                    
             }     
               
        }
        public class MainCallMethodVisitor extends VoidVisitorAdapter{//Visitor to construct the list of function in the main function
            private ArrayList<String> Functionlist = new ArrayList();
            private ArrayList<String> Objectlist = new ArrayList();
            @Override
            public void visit(MethodCallExpr n, Object arg){
                if(n.getScope()!=null){
                        Functionlist.add(n.getScope().toString()+"."+n.getName().toString());
                        Objectlist.add(n.getScope().toString());
                    }else{
                        Functionlist.add("this."+n.getName().toString());
                }
            
             }
            
            public ArrayList<String> getFunctions(){
                return Functionlist;
            }
            public ArrayList<String> getObjects(){
                return Objectlist;
            }
            
        }
        public class CallMethodVisitor extends VoidVisitorAdapter{ //Visitor to construct a list of function for a specific function
            private ArrayList<String> Functionlist = new ArrayList();
            private ArrayList<String> Objectlist = new ArrayList();
            @Override
            public void visit(MethodCallExpr n, Object arg){
               
                if(n.getScope()!=null){
                        Functionlist.add(n.getScope().toString()+"."+n.getName().toString());
                        Objectlist.add(n.getScope().toString());
                    }else{
                        Functionlist.add("this."+n.getName().toString());
                }
            
             }
            public void nullall(){
                Functionlist.clear();
                Objectlist.clear();
            }
            public ArrayList<String> getFunctions(){
                return Functionlist;
            }
            public ArrayList<String> getObjects(){
                return Objectlist;
            }
            
        }
        public class ContentMethodVisitor extends VoidVisitorAdapter{ //Visitor to find main function
            private String MainFunc;
            private String startFunc;
            @Override
            public void visit(MethodDeclaration d, Object arg){
                if(startFunc.equals(d.getName())){
                    MainFunc=d.getBody().toString();
                }
            }
               /* }if("main".equals(d.getName())){
                    MainFunc=d.getBody().toString();
               }*/
            public String getMain(){
                return MainFunc;
            }
            
            public void setStartFunc(String srtFunc){
                startFunc = srtFunc;
            }  
        }
        public class MethodDeclarationVisitor extends VoidVisitorAdapter{ //Visotor to find specific function
            private String Func;
            private String Funcname;
            @Override
            public void visit(MethodDeclaration d, Object arg){
                if(d.getName() == null ? arg.toString() == null : d.getName().equals(arg.toString())){
                    Funcname=d.getName();
                    Func=d.getBody().toString();
                }
                
            }
            public String getFunc(){
                return Func;
            }
            public String getFuncName(){
                return Funcname;
            }
            
        }
         public class InstanceMethodVisitor extends VoidVisitorAdapter{ //Visitor to find an instance of an object
            private String ClassName=null;
            @Override
            public void visit(MethodDeclaration d, Object arg){
                List<Statement> Statements=d.getBody().getStmts();
                for(int i=0; i<Statements.size(); i++){
                    if(Statements.get(i).toString().contains(arg.toString()+" = new")){
                    String[] parts=Statements.get(i).toString().split(" = new");
                    parts=parts[1].split("\\(");
                    ClassName=parts[0];
                    
                    }       
                }    
            }
            public void setnull(){
                ClassName=null;
            }
            public String getClassName(){
                return ClassName;
            }
        }
}

