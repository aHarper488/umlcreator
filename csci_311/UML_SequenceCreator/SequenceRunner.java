/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.UML_SequenceCreator;

import csci_311.FileHandling.fileManager;
import csci_311.Runner;
import csci_311.Structs.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class SequenceRunner {
    
    private fileManager FM = fileManager.getSingleton();
    private ArrayList<aFile> filelist = FM.getFileList();
    //private ArrayList<Sequence> sequenceModel = FM.getSeqList();

    //locates and generates all sequences to their classes/functions
    public void findSequence(){
        
        for(int i=0; i<filelist.size(); i++){
            
            //System.out.println("Searching: " + filelist.get(i).getClassName());
            //if contains and action listner for java gui
            if(filelist.get(i).getContents().contains("actionPerformed")){
                
                System.out.println("Found Action performed In " + filelist.get(i).getClassName());
                try {
                        sequenceReader SequenceReader = new sequenceReader();
                        SequenceReader.findSequence(filelist.get(i), "actionPerformed");
                    } 
                    catch (FileNotFoundException ex) {
                        Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                    catch (japa.parser.ParseException ex) {
                        Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
            }
        }
       
        /*for(int x = 0; x < sequenceModel.size(); x++){
            for(int i=0; i<filelist.size(); i++){
                if(filelist.get(i).getClassName().equals(sequenceModel.get(x).getSeqClass())){
                    
                    System.out.println("Executing Seq:" + sequenceModel.get(x).getSeqFunction() + " in " + sequenceModel.get(x).getSeqClass() + " At marker " + x);
                    
                    try {
                        sequenceReader SequenceReader = new sequenceReader();
                        SequenceReader.findSequence(filelist.get(i), sequenceModel.get(x));
                        break;
                    } 
                    catch (FileNotFoundException ex) {
                        Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                    catch (japa.parser.ParseException ex) {
                        Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
                
        }*/
      
    }
}
