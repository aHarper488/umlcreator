/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.XMI_Builder;

/**
 *
 * @author Daniel
 */
public class Base_XMI_Builder {
    
    //singleton manager
    protected XMI_Manager XMLmgr;
    
    //construction of the xmi string
    protected StringBuffer buildString = new StringBuffer();
    
    //completed output to send out
    protected String output = "";
    
    Base_XMI_Builder(){
        
        XMLmgr = XMI_Manager.getSingleton();
    }
      
    public String getOutput(){
        return output;
    }
}
