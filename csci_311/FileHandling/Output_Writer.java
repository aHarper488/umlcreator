/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csci_311.FileHandling;

import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
/**
 *
 * @author Daniel
 */
public class Output_Writer {
    
    private  FileWriter fileWriter;
    
    public Output_Writer(String input, String path, String outputName) throws IOException{
       File file = new File(path, outputName);
       fileWriter = new FileWriter(file);
       fileWriter.write(input);
       fileWriter.close();
    }
}
